package pl.wipb.ztp.ps2;

import java.util.LinkedList;

public class Kategoria extends Element {

    public int id_dzialu;
    public LinkedList<Typ> typy = new LinkedList<Typ>();

    public Kategoria( int _numer, String _nazwa, int _id_dzialu){
        this.nazwa = _nazwa;
        this.numer = _numer;
        this.id_dzialu = _id_dzialu;
    }
    public Kategoria(int _numer, String _nazwa){
        nazwa = _nazwa;
        numer = _numer;
    }

    @Override
    public String WypiszInformacje() {
        System.out.println("Kategoria: " + nazwa);
        return ("Kategoria: " + nazwa);
    }

    @Override
    public void Lista() {
        for (Typ t : typy) {
            t.WypiszInformacje();
            t.Lista();
        }

    }

    @Override
    public String toString() {
        return nazwa;
    }
}
