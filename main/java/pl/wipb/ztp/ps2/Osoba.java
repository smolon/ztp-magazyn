package pl.wipb.ztp.ps2;

public abstract class Osoba {
    public int id;
    public String imie;
    public String nazwisko;
    public String email;
    public String haslo;
    public int typKonta;

    public Osoba(int id, String imie, String nazwisko, String email, String haslo){
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.id  = id;
        this.email = email;
        this.haslo = haslo;
        this.typKonta = 1;
    }
    public Osoba(int id, String imie, String nazwisko, String email, String haslo, int typ_konta){
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.id  = id;
        this.email = email;
        this.haslo = haslo;
        this.typKonta = typ_konta;
    }
    public String getImie(){
        return imie;
    }
    public String getNazwisko(){
        return nazwisko;
    }
    public int getId() {return id;}
    public  String getEmail() { return email;}
    public abstract void Opis();
}
