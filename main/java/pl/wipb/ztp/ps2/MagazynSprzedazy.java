package pl.wipb.ztp.ps2;

import java.util.LinkedList;

public class MagazynSprzedazy extends Magazyn {
    public LinkedList<Produkt> czesci = new LinkedList<Produkt>();
    public MagazynSprzedazy(int _numer, String _nazwa) {
        super(_numer,_nazwa);
    }

    @Override
    public String GetNazwa() {
        System.out.println("---" + nazwa + "---");
        return nazwa;
    }


    @Override
    public int GetSize() {
        return czesci.size();
    }

    @Override
    public void Print() {

        for (Produkt p : czesci) {
                p.WypiszInformacje();
        }
    }


    @Override
    public Produkt GetElement(String _nazwa) {
        for(Produkt p : czesci){
            if(p.nazwa == _nazwa)
                return  p;
        }
        return null;
    }

    @Override
    public String GetProdukty() {
        for(Produkt p : czesci){
            return p.nazwa;
        }
        return null;
    }
}
