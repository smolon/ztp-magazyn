package pl.wipb.ztp.ps2;

import java.util.LinkedList;

public class Zamowienie {
    public int id_Uzytkownika;
    public int NumerZamowienia;
    public int id_rachunku;
    Rachunek rachunek;
    public double doZaplaty;
    public LinkedList<Produkt> produkty = new LinkedList<Produkt>();
    public LinkedList<Produkt> dodane = new LinkedList<Produkt>();

    public Zamowienie() { };

    public void SposobRozliczenia(Rachunek rachunek){
        this.rachunek = rachunek;
        this.rachunek.SposobRozliczenia(this);
    }
    Zamowienie(int _id, double _cena, LinkedList<Produkt> _produkty, int _id_Uzytkownika, int _id_rachunku){
        NumerZamowienia = _id;
        doZaplaty = _cena;
        produkty = _produkty;
        id_Uzytkownika = _id_Uzytkownika;
        id_rachunku = _id_rachunku;
    }
    public void DodajDoZamowienia(Produkt p){
        produkty.add(p);
    }

    public void UsunZZamowienia(String Nazwa){
        for(Produkt produkt : produkty){
            if(produkt.nazwa.equals(Nazwa))
                produkty.remove(produkt);
        }
    }

    public int GetNumerZamowienia(){
        return NumerZamowienia;
    }
    public double WystawRachunek(Rachunek _r){
        doZaplaty=0;
        doZaplaty = _r.Rozlicz(produkty);
        return doZaplaty;
    }

    public String OpisZamowienia(){
        return "Zamowienie numer: " + GetNumerZamowienia();
    }
    public int Cena(){
        return 0;
    }
}
