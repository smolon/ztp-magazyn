package pl.wipb.ztp.ps2;

import java.util.LinkedList;

public interface Rachunek {
     void SposobRozliczenia(Zamowienie zamowienie);
     double Rozlicz(LinkedList<Produkt> produkty);


}
