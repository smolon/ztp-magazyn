package pl.wipb.ztp.ps2;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

public class Biblioteka {

    public static final String DRIVER = "org.sqlite.JDBC";
    public static final String DB_URL = "jdbc:sqlite:biblioteka.db";

    private Connection conn;
    private Statement stat;

    public Biblioteka() {
        try {
            Class.forName(Biblioteka.DRIVER);
        } catch (ClassNotFoundException e) {
            System.err.println("Brak sterownika JDBC");
            e.printStackTrace();
        }

        try {
            conn = DriverManager.getConnection(DB_URL);
            stat = conn.createStatement();
        } catch (SQLException e) {
            System.err.println("Problem z otwarciem polaczenia");
            e.printStackTrace();
        }

        //createTables();
    }
    //////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////
    public boolean insertAdministrator(String imie, String nazwisko){
        try{
            PreparedStatement preparedStatement = conn.prepareStatement("insert into Administrator values (NULL,?,?);");
            preparedStatement.setString(1, nazwisko);
            preparedStatement.setString(2,imie);
            preparedStatement.execute();
            System.out.println("Dodano");
        } catch (SQLException e){
            return false;
        }
        return true;
    }
    public boolean deleteAdministrator(int id){
        try{
            PreparedStatement preparedStatement = conn.prepareStatement("delete from Administrator where admin_id = ?");
            preparedStatement.setString(1, String.valueOf(id));
            preparedStatement.execute();
            System.out.println("usunieto");
        } catch (SQLException e){
            System.out.println("Błąd przy wstawianiu administratora");
            e.printStackTrace();
            return false;
        }
        return true;
    }
    public List<Administrator> selectAdministratorzy(){
        List<Administrator> administratorzy = new LinkedList<Administrator>();
        try{
            ResultSet resultSet = stat.executeQuery("SELECT * FROM Administrator");
            int id;
            String imie, nazwisko, email, haslo;
            while (resultSet.next()){
                id=resultSet.getInt("admin_id");
                imie = resultSet.getString("imie");
                nazwisko = resultSet.getString("nazwisko");
                email = resultSet.getString("email");
                haslo = resultSet.getString("haslo");
                administratorzy.add(new Administrator(id, imie, nazwisko, email, haslo));
            }
        } catch (SQLException e){
            e.printStackTrace();
            return null;
        }
        return administratorzy;
    }
    //////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////
    public boolean insertDzial(String nazwa){
        try{
            PreparedStatement preparedStatement = conn.prepareStatement("insert into Dzial values (NULL,?);");
            preparedStatement.setString(1, nazwa);
            preparedStatement.execute();
            System.out.println("Dodano dzial");
        } catch (SQLException e){
            return false;
        }
        return true;
    }
    public boolean deleteDzial(int id){
        try{
            PreparedStatement preparedStatement = conn.prepareStatement("delete from Dzial where id_dzial = ?");
            preparedStatement.setString(1, String.valueOf(id));
            preparedStatement.execute();
            System.out.println("usunieto");
        } catch (SQLException e){
            System.out.println("Błąd przy usuwaniu dzialu");
            e.printStackTrace();
            return false;
        }
        return true;
    }
    public List<Dzial> selectDzial(){
        List<Dzial> dzialy = new LinkedList<Dzial>();
        try{
            System.out.println("try");
            ResultSet resultSet = stat.executeQuery("SELECT * FROM Dzial");
            int id;
            String nazwa;
            while (resultSet.next()){
                System.out.println("pobrano wartosc");
                id=resultSet.getInt("id_dzial");
                nazwa = resultSet.getString("nazwa");
                dzialy.add(new Dzial(id, nazwa));
            }
        } catch (SQLException e){
            e.printStackTrace();
            return null;
        }
        return dzialy;
    }
    //////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////

    public boolean insertFakturaNaOsobeFizyczna(String imie, String nazwisko, double cena){
        try{
            PreparedStatement preparedStatement = conn.prepareStatement("insert into FakturaNaOsobeFizyczna values (NULL,?,?,?);");
            preparedStatement.setString(1,imie);
            preparedStatement.setString(2, nazwisko);
            preparedStatement.setString(3, String.valueOf(cena));
            preparedStatement.execute();
            System.out.println("Dodano fakture na osobe fiz");
        } catch (SQLException e){
            return false;
        }
        return true;
    }
    public boolean deleteFakturaNaOsobeFizyczna(int id){
        try{
            PreparedStatement preparedStatement = conn.prepareStatement("delete from FakturaNaOsobeFizyczna where id_fakturyfiz = ?");
            preparedStatement.setString(1, String.valueOf(id));
            preparedStatement.execute();
            System.out.println("usunieto fakture na oosbe fiz");
        } catch (SQLException e){
            System.out.println("Błąd przy usuwaniu fatkruy na osobe fizyczna");
            e.printStackTrace();
            return false;
        }
        return true;
    }
    public List<FakturaNaOsobeFizyczna> selectFakturaNaOsobeFizyczna(){
        List<FakturaNaOsobeFizyczna> fakturyNaOsobe = new LinkedList<FakturaNaOsobeFizyczna>();
        try{
            ResultSet resultSet = stat.executeQuery("SELECT * FROM FakturaNaOsobeFizyczna");
            int id;
            double cena;
            String imie, nazwisko;
            while (resultSet.next()){
                id=resultSet.getInt("id_fakturyfiz");
                imie = resultSet.getString("imie");
                nazwisko = resultSet.getString("nazwisko");
                cena = resultSet.getDouble("cena");
                fakturyNaOsobe.add(new FakturaNaOsobeFizyczna(id, imie, nazwisko, cena));
            }
        } catch (SQLException e){
            e.printStackTrace();
            return null;
        }
        return fakturyNaOsobe;
    }
    //////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////
    public boolean insertFakturaNaFirme(double NIP, double cena){
        try{
            PreparedStatement preparedStatement = conn.prepareStatement("insert into FakturaNaFirme values (NULL,?,?);");
            preparedStatement.setString(1, String.valueOf(NIP));
            preparedStatement.setString(2, String.valueOf(cena));
            preparedStatement.execute();
            System.out.println("Dodano fakture na firme");
        } catch (SQLException e){
            return false;
        }
        return true;
    }
    public boolean deleteFakturaNaFirme(int id){
        try{
            PreparedStatement preparedStatement = conn.prepareStatement("delete from FakturaNaFirme where id_faktury = ?");
            preparedStatement.setString(1, String.valueOf(id));
            preparedStatement.execute();
            System.out.println("usunieto fakture na firme");
        } catch (SQLException e){
            System.out.println("Błąd przy usuwaniu fatkruy na firme");
            e.printStackTrace();
            return false;
        }
        return true;
    }
    public List<FakturaNaFirme> selectFakturaNaFirme(){
        List<FakturaNaFirme> fakturyNaFirme = new LinkedList<FakturaNaFirme>();
        try{
            ResultSet resultSet = stat.executeQuery("SELECT * FROM FakturaNaFirme");
            int id;
            double cena;
            double NIP;
            while (resultSet.next()){
                id=resultSet.getInt("id_faktury");
                cena = resultSet.getDouble("cena");
                NIP = resultSet.getDouble("NIP");
                fakturyNaFirme.add(new FakturaNaFirme(id, cena, NIP));
            }
        } catch (SQLException e){
            e.printStackTrace();
            return null;
        }
        return fakturyNaFirme;
    }
    //////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////
    //--------
    //Tutaj nie wiem, bo firma ma byc tylko jedna to nie wiem czy potrzeban tabela, a jak cos tam probowalem robic tabele
    //to w ogole jakies bledy i w ogole ola boga pomozcie
    public boolean insertFirma(String nazwa, double NIP){
        try{
            PreparedStatement preparedStatement = conn.prepareStatement("insert into Firma values (NULL,?,?);");
            preparedStatement.setString(1, String.valueOf(NIP));
            preparedStatement.setString(2, nazwa);
            preparedStatement.execute();
            System.out.println("Dodano firme");
        } catch (SQLException e){
            return false;
        }
        return true;
    }
    public boolean deleteFirma(int id){
        try{
            PreparedStatement preparedStatement = conn.prepareStatement("delete from Firma where id_firmy = ?");
            preparedStatement.setString(1, String.valueOf(id));
            preparedStatement.execute();
            System.out.println("usunieto  firme");
        } catch (SQLException e){
            System.out.println("Błąd przy usuwaniu firmy");
            e.printStackTrace();
            return false;
        }
        return true;
    }
    public List<Firma> selectFirma(){
        List<Firma> firma = new LinkedList<Firma>();
        try{
            ResultSet resultSet = stat.executeQuery("SELECT * FROM Firma");
            int id;
            String nazwa;
            double NIP;
            while (resultSet.next()){
                id=resultSet.getInt("id_firmy");
                nazwa = resultSet.getString("nazwa");
                NIP = resultSet.getDouble("NIP");
                firma.add(Firma.getInstance().init(nazwa, NIP, id));
            }
        } catch (SQLException e){
            e.printStackTrace();
            return null;
        }
        return firma;
    }
    //////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////
    public boolean insertKategoria(String nazwa, int id_dzialu){
        try{
            PreparedStatement preparedStatement = conn.prepareStatement("insert into Kategoria values (NULL,?,?);");
            preparedStatement.setString(1, nazwa);
            preparedStatement.setString(2, String.valueOf(id_dzialu));
            preparedStatement.execute();
            System.out.println("Dodano kategoria");
        } catch (SQLException e){
            return false;
        }
        return true;
    }
    public boolean deleteKategorie(int id){
        try{
            PreparedStatement preparedStatement = conn.prepareStatement("delete from Kategoria where id_kategorii = ?");
            preparedStatement.setString(1, String.valueOf(id));
            preparedStatement.execute();
            System.out.println("usunieto kategoria");
        } catch (SQLException e){
            System.out.println("Błąd przy usuwaniu kategoria");
            e.printStackTrace();
            return false;
        }
        return true;
    }
    public List<Kategoria> selectKategoria(){
        List<Kategoria> kategorie = new LinkedList<Kategoria>();
        try{
            ResultSet resultSet = stat.executeQuery("SELECT * FROM Kategoria");
            int id;
            String nazwa;
            int id_dzialu;
            while (resultSet.next()){
                id=resultSet.getInt("id_kategorii");
                nazwa = resultSet.getString("nazwa");
                id_dzialu = resultSet.getInt("id_dzialu");
                kategorie.add(new Kategoria(id, nazwa, id_dzialu));
            }
        } catch (SQLException e){
            e.printStackTrace();
            return null;
        }
        return kategorie;
    }
    //////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////


    //////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////
    public boolean insertMagazynReklamacji(String nazwa){
        try{
            PreparedStatement preparedStatement = conn.prepareStatement("insert into MagazynReklamacji values (NULL,?);");
            preparedStatement.setString(1, nazwa);
            preparedStatement.execute();
            System.out.println("Dodano magazyn reklamacji");
        } catch (SQLException e){
            return false;
        }
        return true;
    }
    public boolean deleteMagazynReklamacji(String name){
        try{
            PreparedStatement preparedStatement = conn.prepareStatement("delete from MagazynReklamacji where nazwa = ?");
            preparedStatement.setString(1, String.valueOf(name));
            preparedStatement.execute();
            System.out.println("usunieto magazyn reklamacji");
        } catch (SQLException e){
            System.out.println("Błąd przy usuwaniu magazyn reklamacji");
            e.printStackTrace();
            return false;
        }
        return true;
    }
    public List<MagazynReklamacji> selectMagazynReklamacji(){
        List<MagazynReklamacji> magazynReklamacji = new LinkedList<MagazynReklamacji>();
        try{
            ResultSet resultSet = stat.executeQuery("SELECT * FROM MagazynReklamacji");
            int id;
            String nazwa;
            while (resultSet.next()){
                id=resultSet.getInt("id_magazynurekl");
                nazwa = resultSet.getString("nazwa");
                magazynReklamacji.add(new MagazynReklamacji(id, nazwa));
            }
        } catch (SQLException e){
            e.printStackTrace();
            return null;
        }
        return magazynReklamacji;
    }
    //////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////
    public boolean insertMagazynSprzedazy(String nazwa){
        try{
            PreparedStatement preparedStatement = conn.prepareStatement("insert into MagazynSprzedazy values (NULL,?);");
            preparedStatement.setString(1, nazwa);
            preparedStatement.execute();
            System.out.println("Dodano magazyn sprzedazy");
        } catch (SQLException e){
            return false;
        }
        return true;
    }
    public boolean deleteMagazynSprzedazy(String name){
        try{
            PreparedStatement preparedStatement = conn.prepareStatement("delete from MagazynSprzedazy where nazwa = ?");
            preparedStatement.setString(1, String.valueOf(name));
            preparedStatement.execute();
            System.out.println("usunieto magazyn sprzedzazy");
        } catch (SQLException e){
            System.out.println("Błąd przy usuwaniu magazyn sprzedazy");
            e.printStackTrace();
            return false;
        }
        return true;
    }
    public List<MagazynSprzedazy> selectMagazynSprzedazy(){
        List<MagazynSprzedazy> magazynySprzedazy = new LinkedList<MagazynSprzedazy>();
        try{
            ResultSet resultSet = stat.executeQuery("SELECT * FROM MagazynSprzedazy");
            int id;
            String nazwa;
            while (resultSet.next()){
                id=resultSet.getInt("id_magazynusprz");
                nazwa = resultSet.getString("nazwa");
                magazynySprzedazy.add(new MagazynSprzedazy(id, nazwa));
            }
        } catch (SQLException e){
            e.printStackTrace();
            return null;
        }
        return magazynySprzedazy;
    }
    //////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////
    public boolean insertParagon(double cena){
        try{
            PreparedStatement preparedStatement = conn.prepareStatement("insert into Paragon values (NULL,?);");
            preparedStatement.setString(1, String.valueOf(cena));
            preparedStatement.execute();
            System.out.println("Dodano magazyn paragon");
        } catch (SQLException e){
            return false;
        }
        return true;
    }
    public boolean deleteParagon(int id){
        try{
            PreparedStatement preparedStatement = conn.prepareStatement("delete from Paragon where id_paragonu = ?");
            preparedStatement.setString(1, String.valueOf(id));
            preparedStatement.execute();
            System.out.println("usunieto paragon");
        } catch (SQLException e){
            System.out.println("Błąd przy usuwaniu paragon");
            e.printStackTrace();
            return false;
        }
        return true;
    }
    public List<Paragon> selectParagon(){
        List<Paragon> paragony = new LinkedList<Paragon>();
        try{
            ResultSet resultSet = stat.executeQuery("SELECT * FROM Paragon");
            int id;
            double cena;
            while (resultSet.next()){
                id=resultSet.getInt("id_paragonu");
                cena = resultSet.getDouble("cena");
                paragony.add(new Paragon(id, cena));
            }
        } catch (SQLException e){
            e.printStackTrace();
            return null;
        }
        return paragony;
    }
    //////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////
    public boolean insertProdukt(String nazwa, double cena, int id_typ, int id_magazynu, int id_zamowienia, int ilosc){
        try{
            PreparedStatement preparedStatement = conn.prepareStatement("insert into Produkt values (NULL,?,?,?,?,?,?);");
            preparedStatement.setString(1, nazwa);
            preparedStatement.setString(2, String.valueOf(cena));
            preparedStatement.setString(3, String.valueOf(id_typ));
            preparedStatement.setString(4, String.valueOf(id_magazynu));
            preparedStatement.setString(5, String.valueOf(id_zamowienia));
            preparedStatement.setString(6, String.valueOf(ilosc));
            preparedStatement.execute();
            System.out.println("Dodano produkt");
        } catch (SQLException e){
            return false;
        }
        return true;
    }
    public boolean deleteProdukt(String nazwa){
        try{
            PreparedStatement preparedStatement = conn.prepareStatement("delete from Produkt where nazwa = ?");
            preparedStatement.setString(1, String.valueOf(nazwa));
            preparedStatement.execute();
            System.out.println("usunieto produkt");
        } catch (SQLException e){
            System.out.println("Błąd przy usuwaniu produkt");
            e.printStackTrace();
            return false;
        }
        return true;
    }
    public boolean updateProdukt(int id, String nazwa, double cena, int id_typ, int id_magazynu, int id_zamowienia, int ilosc){
        try{
            String _nazwa = nazwa;
            PreparedStatement preparedStatement = conn.prepareStatement(
                    "UPDATE Produkt SET nazwa=?,cena=?,id_typu=?,id_magazynu=?,id_zamowienia=?,ilosc=? WHERE id_produktu = ?");
            preparedStatement.setString(1, _nazwa);
            preparedStatement.setString(2, String.valueOf(cena));
            preparedStatement.setString(3, String.valueOf(id_typ));
            preparedStatement.setString(4, String.valueOf(id_magazynu));
            preparedStatement.setString(5, String.valueOf(id_zamowienia));
            preparedStatement.setString(6, String.valueOf(ilosc));
            preparedStatement.setString(7, String.valueOf(id));
            preparedStatement.executeUpdate();
            System.out.println("zauktalizowano produkt");
        }catch(SQLException e){
            System.out.println("Błąd przy aktuazlizowaniu prdouktu");
            e.printStackTrace();
            return false;
        }
        return true;
    }
    public LinkedList<Produkt> selectProdukt(){
        LinkedList<Produkt> produkty = new LinkedList<Produkt>();
        try{
            ResultSet resultSet = stat.executeQuery("SELECT * FROM Produkt");
            int id, ilosc, id_typu, id_magazynu, id_zamowienia;
            double cena;
            String nazwa;
            while (resultSet.next()){
                id=resultSet.getInt("id_produktu");
                nazwa = resultSet.getString("nazwa");
                cena = resultSet.getDouble("cena");
                ilosc = resultSet.getInt("ilosc");
                id_typu = resultSet.getInt("id_typu");
                id_magazynu = resultSet.getInt("id_magazynu");
                id_zamowienia = resultSet.getInt("id_zamowienia");
                produkty.add(new Produkt.ProduktBuilder()
                        .set_numer(id)
                        .set_nazwa(nazwa)
                        .set_cena(cena)
                        .set_ilosc(ilosc)
                        .set_id_typu(id_typu)
                        .set_id_magazynu(id_magazynu)
                        .set_id_zamowienie(id_zamowienia)
                        .build());
            }
        } catch (SQLException e){
            e.printStackTrace();
            return null;
        }
        return produkty;
    }
    //////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////
    public boolean insertTyp(String nazwa, int id_kategorii){
        try{
            PreparedStatement preparedStatement = conn.prepareStatement("insert into Typ values (NULL,?,?);");
            preparedStatement.setString(1, nazwa);
            preparedStatement.setString(2,String.valueOf(id_kategorii));
            preparedStatement.execute();
            System.out.println("Dodano Typ");
        } catch (SQLException e){
            return false;
        }
        return true;
    }
    public boolean deleteTyp(int id){
        try{
            PreparedStatement preparedStatement = conn.prepareStatement("delete from Typ where id_typu = ?");
            preparedStatement.setString(1, String.valueOf(id));
            preparedStatement.execute();
            System.out.println("usunieto Typ");
        } catch (SQLException e){
            System.out.println("Błąd przy usuwaniu Typ");
            e.printStackTrace();
            return false;
        }
        return true;
    }
    public List<Typ> selectTyp(){
        List<Typ> typy = new LinkedList<Typ>();
        try{
            ResultSet resultSet = stat.executeQuery("SELECT * FROM Typ");
            int id, cena;
            String nazwa;
            int id_kategorii;
            while (resultSet.next()){
                id=resultSet.getInt("id_typu");
                nazwa = resultSet.getString("nazwa");
                id_kategorii = resultSet.getInt("id_kategorii");
                typy.add(new Typ(id, nazwa, id_kategorii));
            }
        } catch (SQLException e){
            e.printStackTrace();
            return null;
        }
        return typy;
    }
    //////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////
    public boolean insertUzytkownik(String imie, String nazwisko, String email, String haslo, int typ){
        try{
            PreparedStatement preparedStatement = conn.prepareStatement("insert into Uzytkownik values (NULL,?,?,?,?,?);");
            preparedStatement.setString(1, imie);
            preparedStatement.setString(2, nazwisko);
            preparedStatement.setString(3, email);
            preparedStatement.setString(4, haslo);
            preparedStatement.setInt(5, typ);
            preparedStatement.execute();
            System.out.println("Dodano Uzytkownik");
        } catch (SQLException e){
            return false;
        }
        return true;
    }
    public boolean deleteUzytkownik(int id){
        try{
            PreparedStatement preparedStatement = conn.prepareStatement("delete from Uzytkownik where id_uzytkownika = ?");
            preparedStatement.setString(1, String.valueOf(id));
            preparedStatement.execute();
            System.out.println("usunieto Uzytkownik");
        } catch (SQLException e){
            System.out.println("Błąd przy usuwaniu Uzytkownik");
            e.printStackTrace();
            return false;
        }
        return true;
    }
    public List<Uzytkownik> selectUzytkownik(){
        List<Uzytkownik> uzytkownikcy = new LinkedList<Uzytkownik>();
        try{
            ResultSet resultSet = stat.executeQuery("SELECT * FROM Uzytkownik");
            int id, typKonta;
            String imie, nazwisko, email, haslo;
            while (resultSet.next()){
                id=resultSet.getInt("id_uzytkownika");
                imie = resultSet.getString("imie");
                nazwisko = resultSet.getString("nazwisko");
                email = resultSet.getString("email");
                haslo = resultSet.getString("haslo");
                typKonta = resultSet.getInt("typ_konta");
                uzytkownikcy.add(new Uzytkownik(id, imie, nazwisko, email, haslo, typKonta));
            }
        } catch (SQLException e){
            e.printStackTrace();
            return null;
        }
        return uzytkownikcy;
    }
    //////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////
    public boolean insertZamowienie(double doZaplaty, int id_uzytkownika, int id_rachunku){
        try{
            PreparedStatement preparedStatement = conn.prepareStatement("insert into Zamowienie values (NULL,?,?,?);");
            preparedStatement.setString(1, String.valueOf(doZaplaty));
            preparedStatement.setString(2,String.valueOf(id_uzytkownika));
            preparedStatement.setString(3,String.valueOf(id_rachunku));
            preparedStatement.execute();
            System.out.println("Dodano Zamowienie");
        } catch (SQLException e){
            System.out.println("Problem z dodaniem zammowienia");
            return false;
        }
        return true;
    }
    public boolean deleteZamowienie(int id){
        try{
            PreparedStatement preparedStatement = conn.prepareStatement("delete from Zamowienie where id_zamowienia = ?");
            preparedStatement.setString(1, String.valueOf(id));
            preparedStatement.execute();
            System.out.println("usunieto Zamowienie");
        } catch (SQLException e){
            System.out.println("Błąd przy usuwaniu Zamowienie");
            e.printStackTrace();
            return false;
        }
        return true;
    }
    public List<Zamowienie> selectZamowienie(){
        List<Zamowienie> zamowienia = new LinkedList<Zamowienie>();
        LinkedList<Produkt> temp = selectProdukt();
        LinkedList<Produkt> produkty = new LinkedList<>();

        try{
            ResultSet resultSet = stat.executeQuery("SELECT * FROM Zamowienie");
            //ResultSet resultSet1 = stat.executeQuery("SELECT * FROM Produkt WHERE id_zamowienia" + id);
            int id, idUzytkownika,id_rachunku;
            double cena;
            while (resultSet.next()){
                id=resultSet.getInt("id_zamowienia");
                cena = resultSet.getDouble( "dozaplaty");
                idUzytkownika = resultSet.getInt("id_uzytkownika");
                id_rachunku = resultSet.getInt("id_rachunku");
                for(int i = 0; i<temp.size(); i++){
                    if(temp.get(i).id_zamowienie == id){
                        produkty.add(temp.get(i));
                    }
                }
                zamowienia.add(new Zamowienie(id, cena, produkty, idUzytkownika, id_rachunku));
                System.out.println(produkty);
                produkty.clear();
            }
        } catch (SQLException e){
            e.printStackTrace();
            return null;
        }
        return zamowienia;
    }
    //////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////
    public boolean insertZnizki(String kod){
        try{
            PreparedStatement preparedStatement = conn.prepareStatement("insert into Znizki values (NULL,?);");
            preparedStatement.setString(1, kod);
            preparedStatement.execute();
            System.out.println("Dodano Znizki");
        } catch (SQLException e){
            return false;
        }
        return true;
    }
    public boolean deleteZnizki(int id){
        try{
            PreparedStatement preparedStatement = conn.prepareStatement("delete from Znizki where id_znizki = ?");
            preparedStatement.setString(1, String.valueOf(id));
            preparedStatement.execute();
            System.out.println("usunieto Znizki");
        } catch (SQLException e){
            System.out.println("Błąd przy usuwaniu Znizki");
            e.printStackTrace();
            return false;
        }
        return true;
    }
    public List<Znizki> selectZnizki(){
        List<Znizki> znizki = new LinkedList<Znizki>();
        try{
            ResultSet resultSet = stat.executeQuery("SELECT * FROM Znizki");
            int id;
            String kod;
            while (resultSet.next()){
                id=resultSet.getInt("id_znizki");
                kod = resultSet.getString("kodznizkowy");
                znizki.add(new Znizki(id, kod));
            }
        } catch (SQLException e){
            e.printStackTrace();
            return null;
        }
        return znizki;
    }
    //////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////


    public void closeConnection() {
        try {
            conn.close();
        } catch (SQLException e) {
            System.err.println("Problem z zamknieciem polaczenia");
            e.printStackTrace();
        }
    }
}
