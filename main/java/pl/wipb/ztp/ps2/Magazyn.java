package pl.wipb.ztp.ps2;

import java.util.LinkedList;

abstract class Magazyn {
    public int numer;
    public String nazwa;
    public LinkedList<Dzial> dzialy = new LinkedList<Dzial>();

    public Magazyn(int _numer, String _nazwa){
        numer = _numer;
        nazwa = _nazwa;
    }
    public abstract String GetNazwa();
    public abstract int GetSize();
    public abstract void Print();
    public abstract Produkt GetElement(String _nazwa);
    public abstract String GetProdukty();

}

