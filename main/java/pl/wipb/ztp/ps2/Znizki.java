package pl.wipb.ztp.ps2;

import java.util.LinkedList;
import java.util.List;

public class Znizki extends Dodatek {
    public int id_znizki;
    public String kod_znizkowy;
    public LinkedList<Znizki> znizki = new LinkedList<Znizki>();
    public Znizki() {}
    public Znizki(Zamowienie z) {
        super(z);
    }
    public Znizki(int id, String kod){
        id_znizki = id;
        kod_znizkowy = kod;
    }
    public Znizki GetElement(int idx) {
        for (Znizki z : znizki) {
            if (z.id_znizki == idx)
                return z;
        }
        return null;
    }
    @Override
    public String OpisZamowienia() {
        return super.OpisZamowienia() + ", Twoj kod na nastepne zakupy 'sale10%'";
    }

    @Override
    public int Cena() {
        return super.Cena();
    }
    @Override
    public String toString() {
        return "["+id_znizki+"] - "+kod_znizkowy;
    }
}
