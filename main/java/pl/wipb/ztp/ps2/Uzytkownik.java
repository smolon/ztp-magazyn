package pl.wipb.ztp.ps2;

public class Uzytkownik extends Osoba {

    public Uzytkownik(int id, String imie, String nazwisko, String email, String haslo) {
        super(id, imie, nazwisko, email, haslo);
    }
    public Uzytkownik(int id, String imie, String nazwisko, String email, String haslo, int typ_konta) {
        super(id, imie, nazwisko, email, haslo, typ_konta);
    }

    @Override
    public void Opis() {
        System.out.println("Zwykly Uzytkownik");
    }

}
