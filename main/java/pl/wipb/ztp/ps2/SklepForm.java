/*
 * Created by JFormDesigner on Fri Jan 17 10:10:37 CET 2020
 */

package pl.wipb.ztp.ps2;

import java.awt.*;
import java.awt.event.*;
import java.util.LinkedList;
import javax.swing.*;
import javax.swing.GroupLayout;
import javax.swing.event.*;
import javax.swing.plaf.*;

/**
 * @author unknown
 */
public class SklepForm extends JFrame {
    public SklepForm() {
        initComponents();
    }

    private void comboDzialItemStateChanged(ItemEvent e) {
        if (e != null && e.getSource().toString() != null && e.getStateChange() == ItemEvent.SELECTED){
            setComboKategoria();
        }
    }

    private void comboKategoriaItemStateChanged(ItemEvent e) {
        if (e != null && e.getSource().toString() != null && e.getStateChange() == ItemEvent.SELECTED){
            setComboTyp();
        }
    }

    private void comboTypItemStateChanged(ItemEvent e) {
        if (e != null && e.getSource().toString() != null && e.getStateChange() == ItemEvent.SELECTED){
            refreshProdList();
        }
    }

    private void przesylkaBoxStateChanged(ChangeEvent e) {
        // TODO add your code here
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - unknown
        comboDzial = new JComboBox();
        comboKategoria = new JComboBox();
        comboTyp = new JComboBox();
        scrollPane1 = new JScrollPane();
        produktyList = new JList();
        dodZamBtn = new JButton();
        wyszukajBtn = new JButton();
        scrollPane2 = new JScrollPane();
        zamList = new JList();
        zlozZamBtn = new JButton();
        usunZamBtn = new JButton();
        przesylkaBox = new JCheckBox();

        //======== this ========
        setResizable(false);
        setBackground(new Color(102, 102, 102));
        var contentPane = getContentPane();

        //---- comboDzial ----
        comboDzial.addItemListener(e -> comboDzialItemStateChanged(e));

        //---- comboKategoria ----
        comboKategoria.addItemListener(e -> comboKategoriaItemStateChanged(e));

        //---- comboTyp ----
        comboTyp.addItemListener(e -> comboTypItemStateChanged(e));

        //======== scrollPane1 ========
        {
            scrollPane1.setViewportView(produktyList);
        }

        //---- dodZamBtn ----
        dodZamBtn.setText("Dodaj do Zam\u00f3wienia");

        //---- wyszukajBtn ----
        wyszukajBtn.setText("Wyszukaj");

        //======== scrollPane2 ========
        {
            scrollPane2.setViewportView(zamList);
        }

        //---- zlozZamBtn ----
        zlozZamBtn.setText("Z\u0142\u00f3\u017c zam\u00f3wienie");

        //---- usunZamBtn ----
        usunZamBtn.setText("Usu\u0144 z zam\u00f3wienia");

        //---- przesylkaBox ----
        przesylkaBox.setText("Wysy\u0142ka ekspresowa (+5 z\u0142)");
        przesylkaBox.addChangeListener(e -> przesylkaBoxStateChanged(e));

        GroupLayout contentPaneLayout = new GroupLayout(contentPane);
        contentPane.setLayout(contentPaneLayout);
        contentPaneLayout.setHorizontalGroup(
            contentPaneLayout.createParallelGroup()
                .addGroup(contentPaneLayout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                        .addGroup(contentPaneLayout.createSequentialGroup()
                            .addComponent(comboDzial, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                            .addGap(39, 39, 39)
                            .addComponent(comboKategoria, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                            .addGap(52, 52, 52)
                            .addComponent(comboTyp, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                            .addGroup(contentPaneLayout.createSequentialGroup()
                                .addComponent(dodZamBtn)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(wyszukajBtn))
                            .addComponent(scrollPane1, GroupLayout.PREFERRED_SIZE, 370, GroupLayout.PREFERRED_SIZE)))
                    .addGap(18, 18, 18)
                    .addGroup(contentPaneLayout.createParallelGroup()
                        .addComponent(scrollPane2, GroupLayout.DEFAULT_SIZE, 398, Short.MAX_VALUE)
                        .addGroup(contentPaneLayout.createSequentialGroup()
                            .addComponent(usunZamBtn)
                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 138, Short.MAX_VALUE)
                            .addComponent(zlozZamBtn))
                        .addGroup(contentPaneLayout.createSequentialGroup()
                            .addComponent(przesylkaBox)
                            .addGap(0, 0, Short.MAX_VALUE)))
                    .addContainerGap())
        );
        contentPaneLayout.setVerticalGroup(
            contentPaneLayout.createParallelGroup()
                .addGroup(contentPaneLayout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                        .addComponent(comboDzial, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addComponent(comboTyp, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addComponent(comboKategoria, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addComponent(przesylkaBox))
                    .addGap(18, 18, 18)
                    .addGroup(contentPaneLayout.createParallelGroup()
                        .addComponent(scrollPane1, GroupLayout.PREFERRED_SIZE, 311, GroupLayout.PREFERRED_SIZE)
                        .addComponent(scrollPane2))
                    .addGap(18, 18, 18)
                    .addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                        .addComponent(dodZamBtn)
                        .addComponent(wyszukajBtn)
                        .addComponent(zlozZamBtn)
                        .addComponent(usunZamBtn))
                    .addContainerGap(15, Short.MAX_VALUE))
        );
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - unknown
    private JComboBox comboDzial;
    private JComboBox comboKategoria;
    private JComboBox comboTyp;
    private JScrollPane scrollPane1;
    private JList produktyList;
    private JButton dodZamBtn;
    private JButton wyszukajBtn;
    private JScrollPane scrollPane2;
    private JList zamList;
    private JButton zlozZamBtn;
    private JButton usunZamBtn;
    private JCheckBox przesylkaBox;
    // JFormDesigner - End of variables declaration  //GEN-END:variables

    Biblioteka biblioteka = new Biblioteka();
    private DefaultComboBoxModel<String> ComboDzial = new DefaultComboBoxModel<>();
    private DefaultComboBoxModel<String> ComboKategoria = new DefaultComboBoxModel<>();
    private DefaultComboBoxModel<String> ComboTyp = new DefaultComboBoxModel<>();
    private DefaultListModel<String> ProdList = new DefaultListModel<>();
    private DefaultListModel<String> ZamowList = new DefaultListModel<>();
    public static Zamowienie zamowienie = new Zamowienie();

    public void setComboDzial(){
        ComboDzial.removeAllElements();
        for(Dzial d : biblioteka.selectDzial()){
            ComboDzial.addElement(d.toString());
        }
        comboDzial.setModel(ComboDzial);
        setComboKategoria();
    }
    public void setComboKategoria(){
        ComboKategoria.removeAllElements();
        Object dz;
        int ind = 0;
        dz = comboDzial.getModel().getElementAt(comboDzial.getSelectedIndex());
        for(Dzial d : biblioteka.selectDzial()){
            if(dz.equals(d.nazwa))
                ind = d.numer;
        }
        for(Kategoria k : biblioteka.selectKategoria()){
            if(k.id_dzialu == ind)
                ComboKategoria.addElement(k.toString());
        }
        comboKategoria.setModel(ComboKategoria);
        setComboTyp();
    }
    public void setComboTyp(){
        ComboTyp.removeAllElements();
        Object kat;
        int ind = 0;
        kat = comboKategoria.getModel().getElementAt(comboKategoria.getSelectedIndex());
        for(Kategoria k : biblioteka.selectKategoria()){
            if(kat.equals(k.nazwa))
                ind = k.numer;
        }
        for(Typ t : biblioteka.selectTyp()){
            if(t.id_kategorii == ind)
                ComboTyp.addElement(t.toString());
        }
        comboTyp.setModel(ComboTyp);
        refreshProdList();
    }
    public void refreshProdList(){
        ProdList.clear();
        Object typ;
        int ind = 0;
        typ = comboTyp.getModel().getElementAt(comboTyp.getSelectedIndex());
        for(Typ t : biblioteka.selectTyp()){
            if(typ.equals(t.nazwa))
                ind = t.numer;
        }
        for(Produkt p : biblioteka.selectProdukt()){
            if(p.id_typu == ind && p.id_magazynu != 0 && p.ilosc > 0)
                ProdList.addElement(p.nazwa + " , ilość: " + p.ilosc + " , cena: " + p.Cena);
        }
        produktyList.setModel(ProdList);
    }
    public void refreshZamowList(){
        ZamowList.clear();

        for(Produkt p : zamowienie.produkty){
            ZamowList.addElement(p.WypiszInformacje());
        }

        zamList.setModel(ZamowList);
    }

    public SklepForm(String title) {
        super(title);

        initComponents();

        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setComboDzial();
        refreshProdList();
        zamowienie.produkty.clear();
        refreshZamowList();
        zamowienie.doZaplaty = 0;

        dodZamBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Object prod;
                Produkt prod_zam = null;
                String nazwa_p;
                boolean czyByl = false;
                int[] indices = produktyList.getSelectedIndices();
                for(int i=0; i<indices.length; i++){
                    prod = produktyList.getModel().getElementAt(indices[i]);
                    nazwa_p = prod.toString();
                    String[] splited = nazwa_p.split("\\s+");
                    for(Produkt p : biblioteka.selectProdukt()){
                        if(splited[0].equals(p.nazwa) && p.id_magazynu != 0){
                            prod_zam = p;
                        }
                    }
                }
                if(zamowienie.produkty.isEmpty()){
                    czyByl = true;
                    prod_zam.ilosc = 1;
                    zamowienie.DodajDoZamowienia(prod_zam);
                }
                else {
                    for(Produkt p : zamowienie.produkty) {
                        if (prod_zam.nazwa.equals(p.nazwa) && p.id_magazynu != 0) {
                            if(prod_zam.ilosc > p.ilosc){
                                p.ilosc++;
                                czyByl = true;
                            }
                            else
                            {
                                zamowienie.dodane.add(prod_zam);
                            }
                        }
                    }
                }
                if(!czyByl && !zamowienie.dodane.contains(prod_zam)){
                    prod_zam.ilosc = 1;
                    zamowienie.DodajDoZamowienia(prod_zam);
                }

                refreshZamowList();
                refreshProdList();
            }
        });

        wyszukajBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                DefaultListModel<String> model = new DefaultListModel<>();
                Produkt prod_zam = null;
                boolean czyByl = false;

                JTextField nazwa = new JTextField(15);
                JPanel my = new JPanel();
                JPanel myPanel1 = new JPanel();
                JPanel myPanel2 = new JPanel();
                my.setLayout(new BoxLayout(my, BoxLayout.PAGE_AXIS));
                my.add(myPanel1);
                my.add(myPanel2);
                myPanel1.add(new JLabel("Nazwa:"));
                myPanel1.add(nazwa);
                JList list = new JList();
                JButton button = new JButton("Wyszukaj");
                button.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        model.clear();
                        for(Produkt p : biblioteka.selectProdukt()){
                            if(nazwa.getText().equals(p.nazwa) && p.id_magazynu != 0)
                                model.addElement(p.nazwa + " , ilość: " + p.ilosc + " , cena: " + p.Cena);
                        }
                        list.setModel(model);
                    }
                });
                myPanel1.add(button);
                list.setPreferredSize(new Dimension(200, 100));
                JScrollPane sp = new JScrollPane(list);
                myPanel2.add(sp);

                int result = JOptionPane.showConfirmDialog(null, my, "Wyszukaj produkt.", JOptionPane.OK_CANCEL_OPTION);

                if (result == JOptionPane.OK_OPTION) {

                    String[] splited = nazwa.getText().split("\\s+");
                    for(Produkt p : biblioteka.selectProdukt()){
                        if(splited[0].equals(p.nazwa) && p.id_magazynu != 0){
                            prod_zam = p;
                        }
                    }

                    if(zamowienie.produkty.isEmpty()){
                        czyByl = true;
                        prod_zam.ilosc = 1;
                        zamowienie.DodajDoZamowienia(prod_zam);
                    }
                    else {
                        for(Produkt p : zamowienie.produkty) {
                            if (prod_zam.nazwa.equals(p.nazwa) && p.id_magazynu != 0) {
                                if(prod_zam.ilosc > p.ilosc){
                                    p.ilosc++;
                                    czyByl = true;
                                }
                                else
                                {
                                    zamowienie.dodane.add(prod_zam);
                                }
                            }
                        }
                    }
                    if(!czyByl && !zamowienie.dodane.contains(prod_zam)){
                        prod_zam.ilosc = 1;
                        zamowienie.DodajDoZamowienia(prod_zam);
                    }
                }
                refreshZamowList();
                refreshProdList();
            }
        });

        zlozZamBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                double doZaplaty = zamowienie.doZaplaty;
                int idRachunku = 0;
                 LinkedList <Znizki> znizki = new LinkedList<Znizki>();
                 for(Znizki z : biblioteka.selectZnizki()){
                      znizki.add(z);
                  }
                 for(Produkt p : zamowienie.produkty){
                    doZaplaty = doZaplaty + (p.ilosc * p.Cena);
                }
                if(doZaplaty > 500 && doZaplaty < 1000)
                    doZaplaty *= 0.95;
                else if(doZaplaty >= 1000 && doZaplaty < 1500)
                    doZaplaty *= 0.9;
                else if(doZaplaty >= 1500 && doZaplaty < 2000)
                    doZaplaty *= 0.85;
                else if(doZaplaty >= 2000)
                    doZaplaty *= 0.8;

                zamowienie.doZaplaty = doZaplaty;
                DefaultComboBoxModel<String> comboList = new DefaultComboBoxModel<>();
                comboList.addElement("Paragon");
                comboList.addElement("Faktura na osobę fizyczną");
                comboList.addElement("Faktura na firmę");

                JComboBox comboBox = new JComboBox(comboList);

                JPanel myPanel = new JPanel();
                myPanel.add(comboBox);

                int result = JOptionPane.showConfirmDialog(null, myPanel, "Wybierz rodzaj rachunku.", JOptionPane.OK_CANCEL_OPTION);

                if (result == JOptionPane.OK_OPTION) {
                    if(comboBox.getSelectedItem().equals("Paragon")) {
                        biblioteka.insertParagon(doZaplaty);
                        for(Paragon p : biblioteka.selectParagon()){
                            idRachunku = p.id_paragonu;
                        }
                        zamowienie.id_rachunku = idRachunku;
                        biblioteka.insertZamowienie(zamowienie.doZaplaty, 0, zamowienie.id_rachunku);
                        JFrame frame = new PodsumowanieForm("Podsumowanie");
                        frame.setVisible(true);
                        dispose();

                    }
                    if(comboBox.getSelectedItem().equals("Faktura na osobę fizyczną")){
                        JPanel dane = new JPanel();
                        dane.add(new JLabel("Imię:"));
                        JTextField  imie =  new JTextField(15);
                        dane.add(imie);
                        dane.add(new JLabel("Nazwisko:"));
                        JTextField  nazwisko =  new JTextField(15);
                        dane.add(nazwisko);
                        int result_dane = JOptionPane.showConfirmDialog(null, dane, "Wybierz rodzaj rachunku.", JOptionPane.OK_CANCEL_OPTION);
                        if(result == JOptionPane.OK_OPTION){
                            biblioteka.insertFakturaNaOsobeFizyczna(imie.getText(), nazwisko.getText(), doZaplaty);
                            for(FakturaNaOsobeFizyczna f : biblioteka.selectFakturaNaOsobeFizyczna()){
                                idRachunku = f.id;
                            }
                            zamowienie.id_rachunku = idRachunku;
                            biblioteka.insertZamowienie(zamowienie.doZaplaty, 0, zamowienie.id_rachunku);
                            JFrame frame = new PodsumowanieForm("Podsumowanie");
                            frame.setVisible(true);
                            dispose();
                        }
                    }
                    if(comboBox.getSelectedItem().equals("Faktura na firmę")){
                        JPanel dane = new JPanel();
                        dane.add(new JLabel("NIP:"));
                        JTextField  nip =  new JTextField(15);
                        dane.add(nip);
                        int result_dane = JOptionPane.showConfirmDialog(null, dane, "Wybierz rodzaj rachunku.", JOptionPane.OK_CANCEL_OPTION);
                        if(result == JOptionPane.OK_OPTION){
                            biblioteka.insertFakturaNaFirme(doZaplaty, Double.parseDouble(nip.getText()));
                            for(FakturaNaFirme f : biblioteka.selectFakturaNaFirme()){
                                idRachunku = f.id;
                            }
                            zamowienie.doZaplaty = zamowienie.doZaplaty * 0.77;
                            zamowienie.id_rachunku = idRachunku;
                            biblioteka.insertZamowienie(zamowienie.doZaplaty, 0, zamowienie.id_rachunku);
                            JFrame frame = new PodsumowanieForm("Podsumowanie");
                            frame.setVisible(true);
                            dispose();
                        }
                    }
                }
            }
        });

        usunZamBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Object prod;
                String nazwa_p;
                int[] indices = zamList.getSelectedIndices();
                for(int i=0; i<indices.length; i++){
                    prod = zamList.getModel().getElementAt(indices[i]);
                    nazwa_p = prod.toString();
                    String[] splited = nazwa_p.split("\\s+");
                    for(Produkt p : biblioteka.selectProdukt()){
                        if(splited[0].equals(p.nazwa)){
                            zamowienie.UsunZZamowienia(splited[0]);
                        }
                    }
                }
                refreshZamowList();
            }
        });

        przesylkaBox.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                if(e.getStateChange() == ItemEvent.SELECTED) {
                    zamowienie.doZaplaty += 5;
                }
            }
        });
    }
}
