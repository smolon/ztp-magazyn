/*
 * Created by JFormDesigner on Mon Jan 20 13:21:16 CET 2020
 */

package pl.wipb.ztp.ps2;

import javax.swing.*;
import javax.swing.GroupLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.event.*;

/**
 * @author unknown
 */
public class PodsumowanieForm extends JFrame {
    public PodsumowanieForm() {
        initComponents();
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - unknown
        scrollPane1 = new JScrollPane();
        zamList = new JList();
        powrotBtn = new JButton();
        cenaLabel = new JLabel();

        //======== this ========
        var contentPane = getContentPane();

        //======== scrollPane1 ========
        {
            scrollPane1.setViewportView(zamList);
        }

        //---- powrotBtn ----
        powrotBtn.setText("Powr\u00f3t");

        //---- cenaLabel ----
        cenaLabel.setText("text");

        GroupLayout contentPaneLayout = new GroupLayout(contentPane);
        contentPane.setLayout(contentPaneLayout);
        contentPaneLayout.setHorizontalGroup(
            contentPaneLayout.createParallelGroup()
                .addGroup(contentPaneLayout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(scrollPane1, GroupLayout.PREFERRED_SIZE, 380, GroupLayout.PREFERRED_SIZE)
                    .addGap(18, 18, 18)
                    .addGroup(contentPaneLayout.createParallelGroup()
                        .addComponent(cenaLabel, GroupLayout.PREFERRED_SIZE, 133, GroupLayout.PREFERRED_SIZE)
                        .addComponent(powrotBtn))
                    .addContainerGap(146, Short.MAX_VALUE))
        );
        contentPaneLayout.setVerticalGroup(
            contentPaneLayout.createParallelGroup()
                .addGroup(contentPaneLayout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(contentPaneLayout.createParallelGroup()
                        .addComponent(scrollPane1, GroupLayout.DEFAULT_SIZE, 376, Short.MAX_VALUE)
                        .addGroup(contentPaneLayout.createSequentialGroup()
                            .addComponent(cenaLabel, GroupLayout.PREFERRED_SIZE, 32, GroupLayout.PREFERRED_SIZE)
                            .addGap(18, 18, 18)
                            .addComponent(powrotBtn)
                            .addGap(0, 0, Short.MAX_VALUE)))
                    .addContainerGap())
        );
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - unknown
    private JScrollPane scrollPane1;
    private JList zamList;
    private JButton powrotBtn;
    private JLabel cenaLabel;
    // JFormDesigner - End of variables declaration  //GEN-END:variables

    private DefaultListModel<String> ZamowList = new DefaultListModel<>();
    Biblioteka biblioteka = new Biblioteka();

    public void refreshZamowList(){
        ZamowList.clear();

        for(Produkt p : SklepForm.zamowienie.produkty){
            ZamowList.addElement(p.WypiszInformacje());
        }

        zamList.setModel(ZamowList);
    }

    public PodsumowanieForm(String title) {
        super(title);

        initComponents();
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        refreshZamowList();
        int ind = 0;

        for(Zamowienie z : biblioteka.selectZamowienie()){
            ind = z.NumerZamowienia;
        }
        SklepForm.zamowienie.NumerZamowienia = ind;

        cenaLabel.setText("Cena do zapłaty: " + String.valueOf(SklepForm.zamowienie.doZaplaty));

        for(Produkt p_baza : biblioteka.selectProdukt()){
            for(Produkt p : SklepForm.zamowienie.produkty){
                if(p_baza.nazwa.equals(p.nazwa)){
                    biblioteka.updateProdukt(p_baza.numer, p_baza.nazwa, p_baza.Cena, p_baza.id_typu, p_baza.id_magazynu, 0, p_baza.ilosc-p.ilosc);
                    biblioteka.insertProdukt(p.nazwa, p.Cena, p.id_typu, 0, SklepForm.zamowienie.NumerZamowienia, p.ilosc);
                }
            }
        }

        powrotBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFrame frame = new SklepForm("Sklep");
                frame.setVisible(true);
                dispose();
            }
        });

    }
}
