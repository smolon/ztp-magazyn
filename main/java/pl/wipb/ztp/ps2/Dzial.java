package pl.wipb.ztp.ps2;

import java.util.LinkedList;

public class Dzial extends Element {

    public int id_magazynu;
    public LinkedList<Kategoria> kategorie = new LinkedList<Kategoria>();

    public Dzial(int _numer, String _nazwa, int _id_magazynu){
        this.nazwa = _nazwa;
        this.numer = _numer;
        this.id_magazynu = _id_magazynu;
    }
    public Dzial(int _numer, String _nazwa){
        nazwa = _nazwa;
        numer = _numer;
    }


    @Override
    public String WypiszInformacje() {
        System.out.println("Dzial: " + nazwa);
        return ("Dzial: " + nazwa);
    }

    @Override
    public void Lista() {
        for (Kategoria k : kategorie) {
            k.WypiszInformacje();
            k.Lista();
        }
    }
    @Override
    public String toString() {
        return nazwa;
    }
}
