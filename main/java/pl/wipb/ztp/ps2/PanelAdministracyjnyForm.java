/*
 * Created by JFormDesigner on Fri Jan 17 10:23:57 CET 2020
 */

package pl.wipb.ztp.ps2;

import java.awt.event.*;
import javax.swing.*;
import javax.swing.GroupLayout;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * @author unknown
 */
public class PanelAdministracyjnyForm extends JFrame {
    public PanelAdministracyjnyForm() {
        initComponents();
    }

    private void magazynSListValueChanged(ListSelectionEvent e) {
        // TODO add your code here
    }

    private void comboDzialItemStateChanged(ItemEvent e) {
        if (e != null && e.getSource().toString() != null && e.getStateChange() == ItemEvent.SELECTED){
            setComboKategoria();
        }
    }

    private void comboKategoriaItemStateChanged(ItemEvent e) {
        if (e != null && e.getSource().toString() != null && e.getStateChange() == ItemEvent.SELECTED){
            setComboTyp();
        }
    }

    private void comboTypItemStateChanged(ItemEvent e) {
        if (e != null && e.getSource().toString() != null && e.getStateChange() == ItemEvent.SELECTED){
            refreshProdList();
        }
    }

    private void produktyListMouseClicked(MouseEvent e) {
        // TODO add your code here
    }

    private void comboMagItemStateChanged(ItemEvent e) {
        if (e != null && e.getSource().toString() != null && e.getStateChange() == ItemEvent.SELECTED){
            refreshProdRList();
        }
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - unknown
        tabbedPane1 = new JTabbedPane();
        panel1 = new JPanel();
        scrollPane5 = new JScrollPane();
        magazynSList = new JList();
        scrollPane6 = new JScrollPane();
        magazynRList = new JList();
        magazynSBtn = new JButton();
        magazynRBtn = new JButton();
        usunMS = new JButton();
        usunMR = new JButton();
        dodDzialBtn = new JButton();
        dodKatBtn = new JButton();
        dodTypBtn = new JButton();
        dodProdRBtn = new JButton();
        panel2 = new JPanel();
        scrollPane2 = new JScrollPane();
        uzytkownicyList = new JList();
        usunUzytkownikaBtn = new JButton();
        label1 = new JLabel();
        panel3 = new JPanel();
        scrollPane3 = new JScrollPane();
        produktyList = new JList();
        dodProdBtn = new JButton();
        comboMag = new JComboBox();
        comboDzial = new JComboBox();
        comboKategoria = new JComboBox();
        comboTyp = new JComboBox();
        usunProdBtn = new JButton();
        scrollPane4 = new JScrollPane();
        produktyReklamList = new JList();
        usunProdRBtn = new JButton();
        comboMagSprzed = new JComboBox();
        panel4 = new JPanel();
        scrollPane1 = new JScrollPane();
        zamowienieList = new JList();
        usunZamBtn = new JButton();
        label2 = new JLabel();

        //======== this ========
        var contentPane = getContentPane();

        //======== tabbedPane1 ========
        {

            //======== panel1 ========
            {
                panel1.setBorder (new javax. swing. border. CompoundBorder( new javax .swing .border .TitledBorder (new javax. swing
                . border. EmptyBorder( 0, 0, 0, 0) , "JF\u006frmDesi\u0067ner Ev\u0061luatio\u006e", javax. swing. border. TitledBorder
                . CENTER, javax. swing. border. TitledBorder. BOTTOM, new java .awt .Font ("Dialo\u0067" ,java .
                awt .Font .BOLD ,12 ), java. awt. Color. red) ,panel1. getBorder( )) )
                ; panel1. addPropertyChangeListener (new java. beans. PropertyChangeListener( ){ @Override public void propertyChange (java .beans .PropertyChangeEvent e
                ) {if ("borde\u0072" .equals (e .getPropertyName () )) throw new RuntimeException( ); }} )
                ;

                //======== scrollPane5 ========
                {

                    //---- magazynSList ----
                    magazynSList.addListSelectionListener(e -> magazynSListValueChanged(e));
                    scrollPane5.setViewportView(magazynSList);
                }

                //======== scrollPane6 ========
                {
                    scrollPane6.setViewportView(magazynRList);
                }

                //---- magazynSBtn ----
                magazynSBtn.setText("Dodaj Magazyn Sprzedazy");

                //---- magazynRBtn ----
                magazynRBtn.setText("Dodaj Magazyn Reklamacji");

                //---- usunMS ----
                usunMS.setText("Usun");

                //---- usunMR ----
                usunMR.setText("Usun");

                //---- dodDzialBtn ----
                dodDzialBtn.setText("Dodaj Dzial");

                //---- dodKatBtn ----
                dodKatBtn.setText("Dodaj Kategorie");

                //---- dodTypBtn ----
                dodTypBtn.setText("Dodaj Typ");

                //---- dodProdRBtn ----
                dodProdRBtn.setText("Dodaj Produkt");

                GroupLayout panel1Layout = new GroupLayout(panel1);
                panel1.setLayout(panel1Layout);
                panel1Layout.setHorizontalGroup(
                    panel1Layout.createParallelGroup()
                        .addGroup(panel1Layout.createSequentialGroup()
                            .addContainerGap()
                            .addGroup(panel1Layout.createParallelGroup()
                                .addComponent(scrollPane5, GroupLayout.PREFERRED_SIZE, 390, GroupLayout.PREFERRED_SIZE)
                                .addGroup(panel1Layout.createSequentialGroup()
                                    .addComponent(magazynSBtn)
                                    .addGap(18, 18, 18)
                                    .addComponent(usunMS))
                                .addGroup(panel1Layout.createSequentialGroup()
                                    .addComponent(dodDzialBtn)
                                    .addGap(18, 18, 18)
                                    .addComponent(dodKatBtn)
                                    .addGap(18, 18, 18)
                                    .addComponent(dodTypBtn)))
                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(panel1Layout.createParallelGroup()
                                .addComponent(scrollPane6, GroupLayout.DEFAULT_SIZE, 390, Short.MAX_VALUE)
                                .addGroup(panel1Layout.createSequentialGroup()
                                    .addGroup(panel1Layout.createParallelGroup()
                                        .addComponent(dodProdRBtn)
                                        .addGroup(panel1Layout.createSequentialGroup()
                                            .addComponent(magazynRBtn)
                                            .addGap(18, 18, 18)
                                            .addComponent(usunMR)))
                                    .addGap(0, 115, Short.MAX_VALUE)))
                            .addContainerGap())
                );
                panel1Layout.setVerticalGroup(
                    panel1Layout.createParallelGroup()
                        .addGroup(panel1Layout.createSequentialGroup()
                            .addContainerGap()
                            .addGroup(panel1Layout.createParallelGroup()
                                .addComponent(scrollPane5, GroupLayout.PREFERRED_SIZE, 300, GroupLayout.PREFERRED_SIZE)
                                .addComponent(scrollPane6))
                            .addGap(18, 18, 18)
                            .addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                .addComponent(magazynSBtn)
                                .addComponent(magazynRBtn)
                                .addComponent(usunMS)
                                .addComponent(usunMR))
                            .addGap(18, 18, 18)
                            .addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                .addComponent(dodDzialBtn)
                                .addComponent(dodKatBtn)
                                .addComponent(dodTypBtn)
                                .addComponent(dodProdRBtn))
                            .addContainerGap(34, Short.MAX_VALUE))
                );
            }
            tabbedPane1.addTab("Magazyny", panel1);

            //======== panel2 ========
            {

                //======== scrollPane2 ========
                {
                    scrollPane2.setViewportView(uzytkownicyList);
                }

                //---- usunUzytkownikaBtn ----
                usunUzytkownikaBtn.setText("Usu\u0144 u\u017cytkownika");

                //---- label1 ----
                label1.setText("E-mail, imi\u0119, nazwisko");

                GroupLayout panel2Layout = new GroupLayout(panel2);
                panel2.setLayout(panel2Layout);
                panel2Layout.setHorizontalGroup(
                    panel2Layout.createParallelGroup()
                        .addGroup(panel2Layout.createSequentialGroup()
                            .addContainerGap()
                            .addGroup(panel2Layout.createParallelGroup()
                                .addComponent(scrollPane2, GroupLayout.DEFAULT_SIZE, 792, Short.MAX_VALUE)
                                .addGroup(panel2Layout.createSequentialGroup()
                                    .addComponent(usunUzytkownikaBtn)
                                    .addContainerGap(664, Short.MAX_VALUE))
                                .addGroup(panel2Layout.createSequentialGroup()
                                    .addComponent(label1)
                                    .addGap(0, 675, Short.MAX_VALUE))))
                );
                panel2Layout.setVerticalGroup(
                    panel2Layout.createParallelGroup()
                        .addGroup(panel2Layout.createSequentialGroup()
                            .addGap(11, 11, 11)
                            .addComponent(label1)
                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(scrollPane2, GroupLayout.PREFERRED_SIZE, 269, GroupLayout.PREFERRED_SIZE)
                            .addGap(18, 18, 18)
                            .addComponent(usunUzytkownikaBtn)
                            .addContainerGap(86, Short.MAX_VALUE))
                );
            }
            tabbedPane1.addTab("U\u017cytkownicy", panel2);

            //======== panel3 ========
            {

                //======== scrollPane3 ========
                {

                    //---- produktyList ----
                    produktyList.addMouseListener(new MouseAdapter() {
                        @Override
                        public void mouseClicked(MouseEvent e) {
                            produktyListMouseClicked(e);
                        }
                    });
                    scrollPane3.setViewportView(produktyList);
                }

                //---- dodProdBtn ----
                dodProdBtn.setText("Dodaj Produkt");

                //---- comboMag ----
                comboMag.addItemListener(e -> comboMagItemStateChanged(e));

                //---- comboDzial ----
                comboDzial.addItemListener(e -> comboDzialItemStateChanged(e));

                //---- comboKategoria ----
                comboKategoria.addItemListener(e -> comboKategoriaItemStateChanged(e));

                //---- comboTyp ----
                comboTyp.addItemListener(e -> comboTypItemStateChanged(e));

                //---- usunProdBtn ----
                usunProdBtn.setText("Usun Produkt");

                //======== scrollPane4 ========
                {
                    scrollPane4.setViewportView(produktyReklamList);
                }

                //---- usunProdRBtn ----
                usunProdRBtn.setText("Usun Produkt");

                GroupLayout panel3Layout = new GroupLayout(panel3);
                panel3.setLayout(panel3Layout);
                panel3Layout.setHorizontalGroup(
                    panel3Layout.createParallelGroup()
                        .addGroup(panel3Layout.createSequentialGroup()
                            .addContainerGap()
                            .addGroup(panel3Layout.createParallelGroup()
                                .addComponent(scrollPane3, GroupLayout.PREFERRED_SIZE, 426, GroupLayout.PREFERRED_SIZE)
                                .addGroup(panel3Layout.createSequentialGroup()
                                    .addComponent(dodProdBtn)
                                    .addGap(18, 18, 18)
                                    .addComponent(usunProdBtn))
                                .addGroup(panel3Layout.createSequentialGroup()
                                    .addComponent(comboMagSprzed, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                    .addGap(18, 18, 18)
                                    .addComponent(comboDzial, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                    .addGap(18, 18, 18)
                                    .addComponent(comboKategoria, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                    .addGap(18, 18, 18)
                                    .addComponent(comboTyp, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                            .addGap(18, 18, 18)
                            .addGroup(panel3Layout.createParallelGroup()
                                .addGroup(panel3Layout.createSequentialGroup()
                                    .addGroup(panel3Layout.createParallelGroup()
                                        .addComponent(comboMag, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                        .addComponent(usunProdRBtn))
                                    .addGap(0, 238, Short.MAX_VALUE))
                                .addComponent(scrollPane4, GroupLayout.DEFAULT_SIZE, 342, Short.MAX_VALUE))
                            .addContainerGap())
                );
                panel3Layout.setVerticalGroup(
                    panel3Layout.createParallelGroup()
                        .addGroup(panel3Layout.createSequentialGroup()
                            .addContainerGap()
                            .addGroup(panel3Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                .addComponent(comboMagSprzed, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                .addComponent(comboDzial, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                .addComponent(comboKategoria, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                .addComponent(comboTyp, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                .addComponent(comboMag, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                            .addGap(3, 3, 3)
                            .addGroup(panel3Layout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                                .addComponent(scrollPane3, GroupLayout.DEFAULT_SIZE, 309, Short.MAX_VALUE)
                                .addComponent(scrollPane4, GroupLayout.DEFAULT_SIZE, 309, Short.MAX_VALUE))
                            .addGap(18, 18, 18)
                            .addGroup(panel3Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                .addComponent(dodProdBtn)
                                .addComponent(usunProdBtn)
                                .addComponent(usunProdRBtn))
                            .addContainerGap(40, Short.MAX_VALUE))
                );
            }
            tabbedPane1.addTab("Produkty", panel3);

            //======== panel4 ========
            {

                //======== scrollPane1 ========
                {
                    scrollPane1.setViewportView(zamowienieList);
                }

                //---- usunZamBtn ----
                usunZamBtn.setText("Usu\u0144 zam\u00f3wienie");

                //---- label2 ----
                label2.setText("Numer zam\u00f3wienia, cena do zap\u0142aty");

                GroupLayout panel4Layout = new GroupLayout(panel4);
                panel4.setLayout(panel4Layout);
                panel4Layout.setHorizontalGroup(
                    panel4Layout.createParallelGroup()
                        .addGroup(panel4Layout.createSequentialGroup()
                            .addContainerGap()
                            .addGroup(panel4Layout.createParallelGroup()
                                .addComponent(scrollPane1, GroupLayout.DEFAULT_SIZE, 792, Short.MAX_VALUE)
                                .addGroup(panel4Layout.createSequentialGroup()
                                    .addComponent(usunZamBtn)
                                    .addContainerGap(665, Short.MAX_VALUE))
                                .addGroup(panel4Layout.createSequentialGroup()
                                    .addComponent(label2)
                                    .addGap(0, 772, Short.MAX_VALUE))))
                );
                panel4Layout.setVerticalGroup(
                    panel4Layout.createParallelGroup()
                        .addGroup(panel4Layout.createSequentialGroup()
                            .addGap(22, 22, 22)
                            .addComponent(label2)
                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(scrollPane1, GroupLayout.PREFERRED_SIZE, 274, GroupLayout.PREFERRED_SIZE)
                            .addGap(18, 18, 18)
                            .addComponent(usunZamBtn)
                            .addContainerGap(70, Short.MAX_VALUE))
                );
            }
            tabbedPane1.addTab("Zam\u00f3wienia", panel4);
        }

        GroupLayout contentPaneLayout = new GroupLayout(contentPane);
        contentPane.setLayout(contentPaneLayout);
        contentPaneLayout.setHorizontalGroup(
            contentPaneLayout.createParallelGroup()
                .addComponent(tabbedPane1)
        );
        contentPaneLayout.setVerticalGroup(
            contentPaneLayout.createParallelGroup()
                .addComponent(tabbedPane1, GroupLayout.Alignment.TRAILING)
        );
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - unknown
    private JTabbedPane tabbedPane1;
    private JPanel panel1;
    private JScrollPane scrollPane5;
    private JList magazynSList;
    private JScrollPane scrollPane6;
    private JList magazynRList;
    private JButton magazynSBtn;
    private JButton magazynRBtn;
    private JButton usunMS;
    private JButton usunMR;
    private JButton dodDzialBtn;
    private JButton dodKatBtn;
    private JButton dodTypBtn;
    private JButton dodProdRBtn;
    private JPanel panel2;
    private JScrollPane scrollPane2;
    private JList uzytkownicyList;
    private JButton usunUzytkownikaBtn;
    private JLabel label1;
    private JPanel panel3;
    private JScrollPane scrollPane3;
    private JList produktyList;
    private JButton dodProdBtn;
    private JComboBox comboMag;
    private JComboBox comboDzial;
    private JComboBox comboKategoria;
    private JComboBox comboTyp;
    private JButton usunProdBtn;
    private JScrollPane scrollPane4;
    private JList produktyReklamList;
    private JButton usunProdRBtn;
    private JComboBox comboMagSprzed;
    private JPanel panel4;
    private JScrollPane scrollPane1;
    private JList zamowienieList;
    private JButton usunZamBtn;
    private JLabel label2;
    // JFormDesigner - End of variables declaration  //GEN-END:variables

    Biblioteka biblioteka = new Biblioteka();
    private DefaultListModel<String> MagSprzedList = new DefaultListModel<>();
    private DefaultListModel<String> MagReklamList = new DefaultListModel<>();
    private DefaultListModel<String> PracList = new DefaultListModel<>();
    private DefaultListModel<String> ProdList = new DefaultListModel<>();
    private DefaultListModel<String> ZamList = new DefaultListModel<>();
    private DefaultListModel<String> ProdRekList = new DefaultListModel<>();
    private DefaultListModel<String> UzytkownicyList = new DefaultListModel<>();
    private DefaultComboBoxModel<String> ComboMag = new DefaultComboBoxModel<>();
    private DefaultComboBoxModel<String> ComboMagSprzed = new DefaultComboBoxModel<>();
    private DefaultComboBoxModel<String> ComboDzial = new DefaultComboBoxModel<>();
    private DefaultComboBoxModel<String> ComboKategoria = new DefaultComboBoxModel<>();
    private DefaultComboBoxModel<String> ComboTyp = new DefaultComboBoxModel<>();

    public void refreshSprzedazList(){
        MagSprzedList.clear();
        for(Magazyn m : biblioteka.selectMagazynSprzedazy()){
            MagSprzedList.addElement(m.GetNazwa());
        }
        magazynSList.setModel(MagSprzedList);

    }
    public void refreshReklamacjaList(){
        MagReklamList.clear();
        for(Magazyn m : biblioteka.selectMagazynReklamacji()){
            MagReklamList.addElement(m.GetNazwa());
        }
        magazynRList.setModel(MagReklamList);
    }


    public void setComboMag(){
        ComboMag.removeAllElements();
        for(Magazyn m : biblioteka.selectMagazynReklamacji()){
            ComboMag.addElement(m.GetNazwa());
        }
        comboMag.setModel(ComboMag);
    }
    public void setComboMagSprzed(){
        ComboMagSprzed.removeAllElements();
        for(Magazyn m : biblioteka.selectMagazynSprzedazy()){
            ComboMagSprzed.addElement(m.GetNazwa());
        }
        comboMagSprzed.setModel(ComboMagSprzed);
    }
    public void setComboDzial(){
        ComboDzial.removeAllElements();
        for(Dzial d : biblioteka.selectDzial()){
            ComboDzial.addElement(d.toString());
        }
        comboDzial.setModel(ComboDzial);
        setComboKategoria();
    }
    public void setComboKategoria(){
        ComboKategoria.removeAllElements();
        Object dz;
        int ind = 0;
        dz = comboDzial.getModel().getElementAt(comboDzial.getSelectedIndex());
        for(Dzial d : biblioteka.selectDzial()){
            if(dz.equals(d.nazwa))
                ind = d.numer;
        }
        for(Kategoria k : biblioteka.selectKategoria()){
            if(k.id_dzialu == ind)
                ComboKategoria.addElement(k.toString());
        }
        comboKategoria.setModel(ComboKategoria);
        setComboTyp();
    }
    public void setComboTyp(){
        ComboTyp.removeAllElements();
        Object kat;
        int ind = 0;
        kat = comboKategoria.getModel().getElementAt(comboKategoria.getSelectedIndex());
        for(Kategoria k : biblioteka.selectKategoria()){
            if(kat.equals(k.nazwa))
                ind = k.numer;
        }
        for(Typ t : biblioteka.selectTyp()){
            if(t.id_kategorii == ind)
                ComboTyp.addElement(t.toString());
        }
        comboTyp.setModel(ComboTyp);
        refreshProdList();
    }
    public void refreshProdList(){
        ProdList.clear();
        Object typ;
        int ind = 0;
        typ = comboTyp.getModel().getElementAt(comboTyp.getSelectedIndex());
        for(Typ t : biblioteka.selectTyp()){
            if(typ.equals(t.nazwa))
                ind = t.numer;
        }
        for(Produkt p : biblioteka.selectProdukt()){
            if(p.id_typu == ind && p.id_magazynu != 0)
                ProdList.addElement(p.nazwa);//p.nazwa + ", ilość: " + p.ilosc + ", cena: " + p.Cena);
        }
        produktyList.setModel(ProdList);
    }
    public void refreshZamList(){
        ZamList.clear();

        for(Zamowienie z : biblioteka.selectZamowienie()) {
            ZamList.addElement(z.NumerZamowienia + " , " + z.doZaplaty);
        }
        zamowienieList.setModel(ZamList);
    }
    public void refreshProdRList(){
        ProdRekList.clear();
        Object mag;
        int ind = 0;
        mag = comboMag.getModel().getElementAt(comboMag.getSelectedIndex());
        for(Magazyn m : biblioteka.selectMagazynReklamacji()){
            if(mag.equals(m.nazwa))
                ind = m.numer;
        }

        for(Produkt p : biblioteka.selectProdukt()){
            if(p.id_magazynu == ind && p.id_typu == 0)
                ProdRekList.addElement(p.nazwa);//p.nazwa + ", ilość: " + p.ilosc + ", cena: " + p.Cena);
        }
        produktyReklamList.setModel(ProdRekList);
    }
    public void refreshUzytkownicyList(){
        UzytkownicyList.clear();

        for(Uzytkownik u : biblioteka.selectUzytkownik()) {
            UzytkownicyList.addElement(u.email + " , " + u.imie + " " + u.nazwisko);
        }
        uzytkownicyList.setModel(UzytkownicyList);
    }

    public PanelAdministracyjnyForm(String title){
        super(title);

        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        initComponents();

        refreshSprzedazList();
        refreshReklamacjaList();
        setComboMagSprzed();
        setComboMag();
        setComboDzial();
        refreshZamList();
        refreshProdRList();
        refreshUzytkownicyList();

        magazynSBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                JTextField xField = new JTextField(15);

                JPanel myPanel = new JPanel();
                myPanel.add(new JLabel("Nazwa:"));
                myPanel.add(xField);

                int result = JOptionPane.showConfirmDialog(null, myPanel, "Wpisz nazwę magazynu.", JOptionPane.OK_CANCEL_OPTION);
                if (result == JOptionPane.OK_OPTION) {
                    biblioteka.insertMagazynSprzedazy(xField.getText());
                }
                refreshSprzedazList();
            }
        });

        magazynRBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                JTextField xField = new JTextField(15);

                JPanel myPanel = new JPanel();
                myPanel.add(new JLabel("Nazwa:"));
                myPanel.add(xField);

                int result = JOptionPane.showConfirmDialog(null, myPanel, "Wpisz nazwę magazynu.", JOptionPane.OK_CANCEL_OPTION);
                if (result == JOptionPane.OK_OPTION) {
                    biblioteka.insertMagazynReklamacji(xField.getText());
                }
                refreshReklamacjaList();
            }
        });

        usunMS.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Object mag;
                int[] indices = magazynSList.getSelectedIndices();
                for(int i=0; i<indices.length; i++){
                    mag = magazynSList.getModel().getElementAt(indices[i]);
                    for(Magazyn m : biblioteka.selectMagazynSprzedazy()){
                        if(mag.equals(m.nazwa))
                            biblioteka.deleteMagazynSprzedazy(m.nazwa);
                    }
                }
                refreshSprzedazList();
            }
        });

        dodDzialBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //int ind = 0;
                JTextField xField = new JTextField(15);
                //JComboBox comboBox = new JComboBox();

                //ComboDzial.removeAllElements();
//                for(Dzial d : biblioteka.selectDzial()){
//                    ComboDzial.addElement(d.toString());
//                }
//                comboBox.setModel(ComboDzial);

//                Object dz;
//                dz = comboBox.getModel().getElementAt(comboBox.getSelectedIndex());
//                for(Dzial dzial : biblioteka.selectDzial()){
//                    if(dz.equals(dzial.nazwa))
//                        ind = dzial.numer;
//                }

                JPanel myPanel = new JPanel();
                myPanel.add(new JLabel("Nazwa:"));
                myPanel.add(xField);

                int result = JOptionPane.showConfirmDialog(null, myPanel, "Wpisz nazwę działu.", JOptionPane.OK_CANCEL_OPTION);
                if (result == JOptionPane.OK_OPTION) {
                    biblioteka.insertDzial(xField.getText());
                }
                setComboDzial();
            }
        });

        dodKatBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int ind = 0;
                JTextField xField = new JTextField(15);
                JComboBox comboBox = new JComboBox();

                ComboDzial.removeAllElements();
                for(Dzial d : biblioteka.selectDzial()){
                    ComboDzial.addElement(d.toString());
                }
                comboBox.setModel(ComboDzial);

                Object dz;
                dz = comboBox.getModel().getElementAt(comboBox.getSelectedIndex());
                for(Dzial dzial : biblioteka.selectDzial()){
                    if(dz.equals(dzial.nazwa))
                        ind = dzial.numer;
                }

                JPanel myPanel = new JPanel();
                myPanel.add(new JLabel("Nazwa:"));
                myPanel.add(xField);
                myPanel.add(comboBox);

                int result = JOptionPane.showConfirmDialog(null, myPanel, "Wpisz nazwę kategorii.", JOptionPane.OK_CANCEL_OPTION);
                if (result == JOptionPane.OK_OPTION) {
                    biblioteka.insertKategoria(xField.getText(), ind);
                }
                setComboDzial();
            }
        });

        dodTypBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int ind = 0;
                JTextField xField = new JTextField(15);
                JComboBox comboBox = new JComboBox();

                ComboKategoria.removeAllElements();
                for(Kategoria k : biblioteka.selectKategoria()){
                    ComboKategoria.addElement(k.toString());
                }
                comboBox.setModel(ComboKategoria);

                Object kat;

                JPanel myPanel = new JPanel();
                myPanel.add(new JLabel("Nazwa:"));
                myPanel.add(xField);
                myPanel.add(comboBox);

                int result = JOptionPane.showConfirmDialog(null, myPanel, "Wpisz nazwę kategorii.", JOptionPane.OK_CANCEL_OPTION);
                if (result == JOptionPane.OK_OPTION) {
                    kat = comboBox.getModel().getElementAt(comboBox.getSelectedIndex());
                    for(Kategoria k : biblioteka.selectKategoria()){
                        if(kat.equals(k.nazwa))
                            ind = k.numer;
                    }
                    biblioteka.insertTyp(xField.getText(), ind);
                }
                setComboDzial();
            }
        });

        usunMR.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Object mag;
                int[] indices = magazynRList.getSelectedIndices();
                for(int i=0; i<indices.length; i++){
                    mag = magazynRList.getModel().getElementAt(indices[i]);
                    for(Magazyn m : biblioteka.selectMagazynReklamacji()){
                        if(mag.equals(m.nazwa))
                            biblioteka.deleteMagazynReklamacji(m.nazwa);
                    }
                }
                refreshReklamacjaList();
            }
        });

        dodProdBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JTextField nazwa = new JTextField(15);
                JTextField cena = new JTextField(15);
                JTextField ilosc = new JTextField(15);

                JPanel myPanel = new JPanel();
                myPanel.add(new JLabel("Nazwa:"));
                myPanel.add(nazwa);
                myPanel.add(new JLabel("Cena:"));
                myPanel.add(cena);
                myPanel.add(new JLabel("Ilosc:"));
                myPanel.add(ilosc);

                Object mag, typ;
                int ind_magazynu = 0, ind_typ = 0;
                mag = comboMagSprzed.getModel().getElementAt(comboMagSprzed.getSelectedIndex());
                typ = comboTyp.getModel().getElementAt(comboTyp.getSelectedIndex());
                for(Magazyn m : biblioteka.selectMagazynSprzedazy()){
                    if(mag.equals(m.nazwa))
                        ind_magazynu = m.numer;
                }
                for(Typ t : biblioteka.selectTyp()){
                    if(typ.equals(t.nazwa))
                        ind_typ = t.numer;
                }

                int result = JOptionPane.showConfirmDialog(null, myPanel, "Wpisz dane produktu.", JOptionPane.OK_CANCEL_OPTION);
                if (result == JOptionPane.OK_OPTION) {
                    biblioteka.insertProdukt(nazwa.getText(),
                            Integer.parseInt(cena.getText()),
                            ind_typ,
                            ind_magazynu,
                            0,
                            Integer.parseInt(ilosc.getText())
                            );
                }
                refreshProdList();
            }
        });

        usunProdBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Object prod;
                int[] indices = produktyList.getSelectedIndices();
                for(int i=0; i<indices.length; i++){
                    prod = produktyList.getModel().getElementAt(indices[i]);
                    for(Produkt p : biblioteka.selectProdukt()){
                        if(prod.equals(p.nazwa))
                            biblioteka.deleteProdukt(p.nazwa);
                    }
                }
                refreshProdList();
            }
        });

        produktyList.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if(e.getClickCount() == 2){
                    int ind = 0;
                    JTextField nazwaField = new JTextField(15);
                    JTextField cenaField = new JTextField(15);
                    JTextField iloscField = new JTextField(15);

                    JPanel myPanel = new JPanel();
                    myPanel.add(new JLabel("Nazwa:"));
                    myPanel.add(nazwaField);
                    myPanel.add(new JLabel("Cena:"));
                    myPanel.add(cenaField);
                    myPanel.add(new JLabel("Ilosc:"));
                    myPanel.add(iloscField);
                    Produkt produkt = null;

                    for(Produkt p : biblioteka.selectProdukt()){
                        if(produktyList.getSelectedValue().equals(p.nazwa)){
                            nazwaField.setText(p.nazwa);
                            cenaField.setText(String.valueOf(p.Cena));
                            iloscField.setText(String.valueOf(p.ilosc));
                            ind = p.numer;
                            System.out.println(ind);
                            produkt = p;
                        }

                    }

                    int result = JOptionPane.showConfirmDialog(null, myPanel, "Dane produktu.", JOptionPane.OK_CANCEL_OPTION);
                    if (result == JOptionPane.OK_OPTION) {
                        produkt.nazwa = nazwaField.getText();
                        produkt.Cena = Double.parseDouble(cenaField.getText());
                        produkt.ilosc = Integer.parseInt(iloscField.getText());

                        biblioteka.updateProdukt(ind, produkt.nazwa, produkt.Cena, produkt.id_typu, produkt.id_magazynu, produkt.id_zamowienie, produkt.ilosc);
                    }
                    refreshProdList();
                }

            }
        });

        dodProdRBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JTextField nazwa = new JTextField(15);
                JTextField ilosc = new JTextField(15);

                JPanel myPanel = new JPanel();
                myPanel.add(new JLabel("Nazwa:"));
                myPanel.add(nazwa);
                myPanel.add(new JLabel("Ilosc:"));
                myPanel.add(ilosc);

                int ind_magazynu = 0;

                Object mag;
                int[] indices = magazynRList.getSelectedIndices();
                for(int i=0; i<indices.length; i++){
                    mag = magazynRList.getModel().getElementAt(indices[i]);
                    for(Magazyn m : biblioteka.selectMagazynReklamacji()){
                        if(mag.equals(m.nazwa))
                            ind_magazynu = m.numer;
                    }
                }

                int result = JOptionPane.showConfirmDialog(null, myPanel, "Wpisz dane produktu.", JOptionPane.OK_CANCEL_OPTION);
                if (result == JOptionPane.OK_OPTION) {
                    biblioteka.insertProdukt(nazwa.getText(), 0, 0, ind_magazynu, 0, Integer.parseInt(ilosc.getText())
                    );
                }
                refreshProdRList();
            }
        });

        usunProdRBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Object prod;
                int[] indices = produktyList.getSelectedIndices();
                for(int i=0; i<indices.length; i++){
                    prod = produktyList.getModel().getElementAt(indices[i]);
                    for(Produkt p : biblioteka.selectProdukt()){
                        if(prod.equals(p.nazwa))
                            biblioteka.deleteProdukt(p.nazwa);
                    }
                }
                refreshProdRList();
            }
        });

        usunZamBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Object zam;
                String nazwa;
                int[] indices = zamowienieList.getSelectedIndices();
                for(int i=0; i<indices.length; i++){
                    zam = zamowienieList.getModel().getElementAt(indices[i]);
                    nazwa = zam.toString();
                    String[] splited = nazwa.split("\\s+");
                    for(Zamowienie z : biblioteka.selectZamowienie()){
                        if(Integer.parseInt(splited[0]) == z.NumerZamowienia){
                            biblioteka.deleteZamowienie(z.NumerZamowienia);
                        }
                    }
                }
                refreshZamList();
            }
        });

        produktyReklamList.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if(e.getClickCount() == 2){
                    int ind = 0;
                    JTextField nazwaField = new JTextField(15);
                    JTextField iloscField = new JTextField(15);

                    JPanel myPanel = new JPanel();
                    myPanel.add(new JLabel("Nazwa:"));
                    myPanel.add(nazwaField);
                    myPanel.add(new JLabel("Ilosc:"));
                    myPanel.add(iloscField);
                    Produkt produkt = null;

                    for(Produkt p : biblioteka.selectProdukt()){
                        if(produktyReklamList.getSelectedValue().equals(p.nazwa)){
                            nazwaField.setText(p.nazwa);
                            iloscField.setText(String.valueOf(p.ilosc));
                            ind = p.numer;
                            System.out.println(ind);
                            produkt = p;
                        }

                    }

                    int result = JOptionPane.showConfirmDialog(null, myPanel, "Dane produktu.", JOptionPane.OK_CANCEL_OPTION);
                    if (result == JOptionPane.OK_OPTION) {
                        produkt.nazwa = nazwaField.getText();
                        produkt.ilosc = Integer.parseInt(iloscField.getText());

                        biblioteka.updateProdukt(ind, produkt.nazwa, produkt.Cena, produkt.id_typu, produkt.id_magazynu, produkt.id_zamowienie, produkt.ilosc);
                    }
                    refreshProdRList();
                }

            }
        });

        usunUzytkownikaBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Object user;
                String nazwa, email, imie, nazwisko;
                int[] indices = uzytkownicyList.getSelectedIndices();
                for(int i=0; i<indices.length; i++){
                    user = uzytkownicyList.getModel().getElementAt(indices[i]);
                    nazwa = user.toString();
                    String[] splited = nazwa.split("\\s+");
                    email = splited[0];
                    imie = splited[2];
                    nazwisko = splited[3];
                    for(Uzytkownik u : biblioteka.selectUzytkownik()){
                        if(u.imie.equals(imie) && u.nazwisko.equals(nazwisko) && u.email.equals(email)){
                            biblioteka.deleteUzytkownik(u.id);
                        }
                    }
                }
                refreshUzytkownicyList();
            }
        });
    }

}
