package pl.wipb.ztp.ps2;
//jdbc:sqlite:identifier.sqlite
import javax.swing.*;
import javax.swing.JFrame;
import java.awt.event.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

//Singleton
final class Firma {

    private double NIP;
    private String Nazwa;
    private int id;

    private static Firma firma = null;
    private LinkedList<Zamowienie> zamowienia = new LinkedList<Zamowienie>();
    public LinkedList<Magazyn> magazyny = new LinkedList<Magazyn>();
    public LinkedList<Produkt> produkty = new LinkedList<Produkt>();

    private Firma() {}
    public static Firma getInstance(){
        if(firma == null)
            firma = new Firma();
        return firma;
    }
    public Firma init(String nazwa, double nip, int id){
        this.Nazwa = nazwa;
        this.NIP = nip;
        this.id = id;
        return firma;
    }
    public double GetNIP() {
        return NIP;
    }

    public String GetNazwa() {
        return Nazwa;
    }

    public static void main(String[] args) {

        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                JFrame frame = new LogowanieForm("Zaloguj się.");
            }


        });



        Biblioteka b = new Biblioteka();

        Firma firma;
        firma=Firma.getInstance();
        firma.produkty = b.selectProdukt();

/*
        List<Administrator> administratorzy = b.selectAdministratorzy();
        System.out.println("Lista administratorów:");
        for(Administrator a: administratorzy)
            System.out.println(a);

        administratorzy = b.selectAdministratorzy();
        System.out.println("Lista administratorów:");
        for(Administrator a: administratorzy)
            System.out.println(a);
        List<Dzial> dzialy = b.selectDzial();

        System.out.println("LIsta działów:");
        for(Dzial d: dzialy)
            System.out.println(d);

        List<Firma> firmatest = b.selectFirma();
        System.out.println("Nazwa firmy:");
        System.out.println(String.valueOf(firmatest.get(0).NIP));
        List<FakturaNaFirme> faktury1 = b.selectFakturaNaFirme();

        System.out.println("Lista faktur na firme: ");
        for(FakturaNaFirme f1: faktury1)
            System.out.println(f1);

        List<Znizki> znizki = b.selectZnizki();
        for(Znizki z: znizki)
            System.out.println(z);
        System.out.println("-----------------------");
        System.out.println("\n\n\n");

        LinkedList<Produkt> prods = b.selectProdukt();
        for(Produkt p: prods)
            System.out.println(">>>>>>PRODUKTY:    " + p.toString());
        List<Zamowienie> zamowienia = b.selectZamowienie();

        for(Zamowienie z1: zamowienia)
            System.out.println(z1.produkty.toString());

 */
        b.closeConnection();


    }
    //Metody do bazy danych
    public static final String DRIVER = "org.sqlite.JDBC";
    public static final String DB_URL = "jdbc:sqlite:biblioteka.db";
    private static Connection conn;
    private Statement stat;

}