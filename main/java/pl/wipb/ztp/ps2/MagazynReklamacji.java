package pl.wipb.ztp.ps2;

import java.util.LinkedList;

public class MagazynReklamacji extends Magazyn {
    public LinkedList<Produkt> doReklamacji = new LinkedList<Produkt>();
    public MagazynReklamacji(int _numer, String _nazwa) {
        super(_numer,_nazwa);
    }

    @Override
    public String GetNazwa() {
        System.out.println("---" + nazwa + "---");
        return nazwa;
    }

    @Override
    public int GetSize() {
        return doReklamacji.size();
    }

    @Override
    public void Print() {
        for (Produkt p : doReklamacji) {
                p.WypiszInformacje();
        }
    }


    @Override
    public Produkt GetElement(String nazwa) {
        for(Produkt p : doReklamacji){
            if(p.nazwa == nazwa)
                return  p;
        }
        return null;
    }

    @Override
    public String GetProdukty() {
        for(Produkt p : doReklamacji){
            return p.nazwa;
        }
        return null;
    }
}
