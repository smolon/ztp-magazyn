/*
 * Created by JFormDesigner on Mon Jan 20 23:27:55 CET 2020
 */

package pl.wipb.ztp.ps2;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.GroupLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * @author unknown
 */
public class RejestracjaForm extends JFrame {
    public RejestracjaForm() {
        initComponents();
    }

    private void zalogujLabelMouseClicked(MouseEvent e) {
        JFrame frame = new LogowanieForm("Zaloguj się.");
        frame.setVisible(true);
        dispose();
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - unknown
        emailField = new JTextField();
        label1 = new JLabel();
        label2 = new JLabel();
        label3 = new JLabel();
        nazwiskoField = new JTextField();
        label4 = new JLabel();
        rejBtn = new JButton();
        imieField = new JTextField();
        label5 = new JLabel();
        hasloField = new JPasswordField();
        zalogujLabel = new JLabel();

        //======== this ========
        var contentPane = getContentPane();

        //---- label1 ----
        label1.setText("E-mail:");

        //---- label2 ----
        label2.setText("Imi\u0119:");

        //---- label3 ----
        label3.setText("Has\u0142o:");

        //---- label4 ----
        label4.setText("Zarejestruj si\u0119!");
        label4.setFont(label4.getFont().deriveFont(label4.getFont().getSize() + 10f));

        //---- rejBtn ----
        rejBtn.setText("Rejestruj");

        //---- label5 ----
        label5.setText("Nazwisko:");

        //---- zalogujLabel ----
        zalogujLabel.setText("Masz ju\u017c konto? Zaloguj si\u0119!");
        zalogujLabel.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                zalogujLabelMouseClicked(e);
            }
        });

        GroupLayout contentPaneLayout = new GroupLayout(contentPane);
        contentPane.setLayout(contentPaneLayout);
        contentPaneLayout.setHorizontalGroup(
            contentPaneLayout.createParallelGroup()
                .addGroup(GroupLayout.Alignment.TRAILING, contentPaneLayout.createSequentialGroup()
                    .addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                        .addGroup(contentPaneLayout.createSequentialGroup()
                            .addGap(50, 50, 50)
                            .addComponent(label1, GroupLayout.DEFAULT_SIZE, 114, Short.MAX_VALUE)
                            .addGap(18, 18, 18)
                            .addComponent(emailField, GroupLayout.PREFERRED_SIZE, 256, GroupLayout.PREFERRED_SIZE))
                        .addGroup(contentPaneLayout.createSequentialGroup()
                            .addContainerGap(50, Short.MAX_VALUE)
                            .addGroup(contentPaneLayout.createParallelGroup()
                                .addGroup(contentPaneLayout.createSequentialGroup()
                                    .addComponent(label2, GroupLayout.PREFERRED_SIZE, 114, GroupLayout.PREFERRED_SIZE)
                                    .addGap(18, 18, 18)
                                    .addComponent(imieField, GroupLayout.PREFERRED_SIZE, 256, GroupLayout.PREFERRED_SIZE))
                                .addGroup(contentPaneLayout.createSequentialGroup()
                                    .addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                                        .addComponent(label3, GroupLayout.PREFERRED_SIZE, 114, GroupLayout.PREFERRED_SIZE)
                                        .addComponent(label5, GroupLayout.PREFERRED_SIZE, 114, GroupLayout.PREFERRED_SIZE))
                                    .addGap(18, 18, 18)
                                    .addGroup(contentPaneLayout.createParallelGroup()
                                        .addComponent(zalogujLabel)
                                        .addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                                            .addComponent(nazwiskoField, GroupLayout.DEFAULT_SIZE, 256, Short.MAX_VALUE)
                                            .addComponent(rejBtn)
                                            .addComponent(hasloField, GroupLayout.DEFAULT_SIZE, 256, Short.MAX_VALUE)))))))
                    .addGap(45, 45, 45))
                .addGroup(GroupLayout.Alignment.TRAILING, contentPaneLayout.createSequentialGroup()
                    .addGap(0, 148, Short.MAX_VALUE)
                    .addComponent(label4, GroupLayout.PREFERRED_SIZE, 200, GroupLayout.PREFERRED_SIZE)
                    .addGap(135, 135, 135))
        );
        contentPaneLayout.setVerticalGroup(
            contentPaneLayout.createParallelGroup()
                .addGroup(contentPaneLayout.createSequentialGroup()
                    .addGap(35, 35, 35)
                    .addComponent(label4, GroupLayout.PREFERRED_SIZE, 43, GroupLayout.PREFERRED_SIZE)
                    .addGap(30, 30, 30)
                    .addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                        .addComponent(emailField)
                        .addComponent(label1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGap(18, 18, 18)
                    .addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                        .addComponent(label2, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(imieField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGap(18, 18, 18)
                    .addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                        .addComponent(nazwiskoField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addComponent(label5, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE))
                    .addGap(18, 18, 18)
                    .addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                        .addComponent(label3, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                        .addComponent(hasloField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGap(18, 18, 18)
                    .addComponent(rejBtn)
                    .addGap(18, 18, 18)
                    .addComponent(zalogujLabel)
                    .addContainerGap(43, Short.MAX_VALUE))
        );
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - unknown
    private JTextField emailField;
    private JLabel label1;
    private JLabel label2;
    private JLabel label3;
    private JTextField nazwiskoField;
    private JLabel label4;
    private JButton rejBtn;
    private JTextField imieField;
    private JLabel label5;
    private JPasswordField hasloField;
    private JLabel zalogujLabel;
    // JFormDesigner - End of variables declaration  //GEN-END:variables

    Biblioteka biblioteka = new Biblioteka();

    public RejestracjaForm(String title) {
        super(title);

        initComponents();
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        rejBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                biblioteka.insertUzytkownik(imieField.getText(), nazwiskoField.getText(), emailField.getText(), hasloField.getText(), 1);


                JFrame frame = new LogowanieForm("Zaloguj się.");
                frame.setVisible(true);
                dispose();
            }
        });
    }
}
