package pl.wipb.ztp.ps2;

import javax.swing.*;
import javax.swing.JFrame;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class StartForm extends JFrame{

    private JPanel panel1;
    private JButton sklep;
    private JButton panelAdministracyjny;

    public StartForm(String title){
        super(title);

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setContentPane(panel1);
        setSize(600, 450);
        setVisible(true);

        panelAdministracyjny.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                JFrame frame = new PanelAdministracyjnyForm("Panel Administracyjny");
                frame.setVisible(true);
                dispose();
            }
        });

        sklep.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFrame frame = new SklepForm("Sklep");
                frame.setVisible(true);
                dispose();
            }
        });
    }
}
