package pl.wipb.ztp.ps2;

abstract class Element {
    public String nazwa;
    public int numer;

    public abstract String WypiszInformacje();
    public abstract void Lista();
}
