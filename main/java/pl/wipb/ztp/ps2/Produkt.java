package pl.wipb.ztp.ps2;

public class Produkt extends Element {

    public int numer;
    public String nazwa;
    public double Cena = 0;
    public int ilosc = 0;
    public int id_typu;
    public int id_magazynu;
    public int id_zamowienie;

    private Produkt(int _numer, String _nazwa, double _cena, int _ilosc, int _id_typu, int _id_magazynu, int _id_zamowienie) {
        numer = _numer;
        nazwa = _nazwa;
        Cena = _cena;
        ilosc = _ilosc;
        id_typu = _id_typu;
        id_magazynu = _id_magazynu;
        id_zamowienie = _id_zamowienie;
    }
    public static class ProduktBuilder {
        private int numer;
        private String nazwa;
        private double cena;
        private int ilosc;
        private int id_typu;
        private int id_magazynu;
        private int id_zamowienie;

        public ProduktBuilder set_numer(int numer) {
            this.numer = numer;
            return this;
        }

        public ProduktBuilder set_nazwa(String nazwa) {
            this.nazwa = nazwa;
            return this;
        }

        public ProduktBuilder set_cena(double cena) {
            this.cena = cena;
            return this;
        }

        public ProduktBuilder set_ilosc(int ilosc) {
            this.ilosc = ilosc;
            return this;
        }

        public ProduktBuilder set_id_typu(int id_typu) {
            this.id_typu = id_typu;
            return this;
        }

        public ProduktBuilder set_id_magazynu(int id_magazynu) {
            this.id_magazynu = id_magazynu;
            return this;
        }

        public ProduktBuilder set_id_zamowienie(int id_zamowienie) {
            this.id_zamowienie = id_zamowienie;
            return this;
        }

        public Produkt build() {
            return new Produkt(numer, nazwa, cena, ilosc, id_typu, id_magazynu, id_zamowienie);
        }
    }
    public double getCena(){
        return this.Cena;
    }
    public int getIlosc(){
        return this.ilosc;
    }
    public String getNazwa(){
        return this.nazwa;
    }
    @Override
    public String WypiszInformacje() {
        System.out.println("Produkt: " + nazwa + " o wartości " + Cena);
        return (nazwa + " o wartości " + Cena + " , ilość: " + ilosc);
    }

    @Override
    public String toString() {
        return "["+numer+"] - "+nazwa+" - "+Cena + "-" + id_zamowienie;
    }
    @Override
    public void Lista() { }

}
