package pl.wipb.ztp.ps2;

public abstract class Dodatek extends Zamowienie {
    private Zamowienie zamowienie;

    public Dodatek() {}
    public Dodatek(Zamowienie z){
        zamowienie = z;
    }
    public String OpisZamowienia(){
        return zamowienie.OpisZamowienia();
    }
    public int Cena(){
        return zamowienie.Cena();
    }
}
