package pl.wipb.ztp.ps2;

public class Administrator extends Uzytkownik {

    public Administrator(int id, String imie, String nazwisko, String email, String haslo) {

        super(id, imie, nazwisko, email, haslo);
    }

    @Override
    public void Opis() {
        System.out.println("Administrator");
    }

    @Override
    public String toString() {
        return "["+getId()+"] - "+getImie()+" - "+getNazwisko();
    }
}
