package pl.wipb.ztp.ps2;

import java.util.LinkedList;

public class Typ extends Element {

    public int ilosc;
    public int id_kategorii;
    public LinkedList<Produkt> produkty = new LinkedList<Produkt>();

    public Typ( int _numer, String _nazwa, int _id_kategorii){
        this.nazwa = _nazwa;
        this.numer = _numer;
        this.id_kategorii = _id_kategorii;
    }

    public Typ(int numer, String nazwa){
        this.nazwa = nazwa;
        this.numer = numer;
    }

    public String getNazwa(){
        return this.nazwa;
    }

    @Override
    public String WypiszInformacje() {
        System.out.println("Typ produktu: " + nazwa);
        return  ("Typ produktu: " + nazwa);
    }

    @Override
    public void Lista(){
        for (Produkt p : produkty) {
            p.WypiszInformacje();
            p.Lista();
        }

    }

    @Override
    public String toString() {
        return nazwa;
    }
}
