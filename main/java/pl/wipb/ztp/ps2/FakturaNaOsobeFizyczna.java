package pl.wipb.ztp.ps2;

import java.util.LinkedList;

public class FakturaNaOsobeFizyczna implements Rachunek {
    public String imie;
    public String nazwisko;
    public int id;
    public double cenaDoZaplaty;
    FakturaNaOsobeFizyczna(int _id, String _imie, String _nazwisko, double _cena){
        id = _id;
        imie = _imie;
        nazwisko = _nazwisko;
        cenaDoZaplaty = _cena;
    }
    @Override
    public void SposobRozliczenia(Zamowienie zamowienie) {
        System.out.println("Faktura na osobe:---");
        System.out.println(zamowienie.WystawRachunek(this));
    }

    @Override
    public double Rozlicz(LinkedList<Produkt> produkty) {
        cenaDoZaplaty = 0;
        for (Produkt p : produkty){
            cenaDoZaplaty += p.getCena();
        }
        return cenaDoZaplaty-2;
    }
}
