package pl.wipb.ztp.ps2;

public class WysylkaEkspresowa extends Dodatek {
    public WysylkaEkspresowa(Zamowienie z) {
        super(z);
    }


    @Override
    public String OpisZamowienia() {
        return super.OpisZamowienia() + ", Wysyłka ekspresowa";
    }

    @Override
    public int Cena() {
        return super.Cena() + 5;
    }
}
