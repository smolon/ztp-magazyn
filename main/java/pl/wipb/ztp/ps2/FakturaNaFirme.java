package pl.wipb.ztp.ps2;

import java.util.LinkedList;

public class FakturaNaFirme implements Rachunek {
    public double cenaDoZaplaty;
    public double NIP;
    public int id;

    FakturaNaFirme(int _id, double _cenaDoZaplaty,  double _NIP){
        cenaDoZaplaty = _cenaDoZaplaty;
        NIP = _NIP;
        id = _id;
    }
    @Override
    public void SposobRozliczenia(Zamowienie zamowienie) {
        System.out.println("Faktura na firme:---");
        System.out.println(zamowienie.WystawRachunek(this));
    }

    @Override
    public double Rozlicz(LinkedList<Produkt> produkty) {
        cenaDoZaplaty = 0;
        for (Produkt p : produkty){
            cenaDoZaplaty += p.getCena();
        }
        return cenaDoZaplaty-2;
    }
    @Override
    public String toString() {
        return "["+id+"] - "+cenaDoZaplaty+" - "+NIP;
    }
}
