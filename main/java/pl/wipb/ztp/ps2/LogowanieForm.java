/*
 * Created by JFormDesigner on Thu Jan 16 22:12:22 CET 2020
 */

package pl.wipb.ztp.ps2;

import java.awt.*;
import java.awt.event.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import javax.swing.GroupLayout;

/**
 * @author unknown
 */
public class LogowanieForm extends JFrame {
    public LogowanieForm() {
        initComponents();
    }

    private void zarejestrujLabelMouseClicked(MouseEvent e) {
        JFrame frame = new RejestracjaForm("Zarejestruj się.");
        frame.setVisible(true);
        dispose();
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - unknown
        label1 = new JLabel();
        emailField = new JTextField();
        label2 = new JLabel();
        hasloField = new JPasswordField();
        panel1 = new JPanel();
        label3 = new JLabel();
        zalogujBtn = new JButton();
        zarejestrujLabel = new JLabel();

        //======== this ========
        setResizable(false);
        setName("this");
        var contentPane = getContentPane();

        //---- label1 ----
        label1.setText("E-mail:");
        label1.setFont(label1.getFont().deriveFont(label1.getFont().getSize() + 4f));
        label1.setName("label1");

        //---- emailField ----
        emailField.setFont(emailField.getFont().deriveFont(emailField.getFont().getSize() + 4f));
        emailField.setName("emailField");

        //---- label2 ----
        label2.setText("Has\u0142o:");
        label2.setFont(label2.getFont().deriveFont(label2.getFont().getSize() + 4f));
        label2.setName("label2");

        //---- hasloField ----
        hasloField.setFont(hasloField.getFont().deriveFont(hasloField.getFont().getSize() + 4f));
        hasloField.setName("hasloField");

        //======== panel1 ========
        {
            panel1.setName("panel1");
            panel1.setBorder (new javax. swing. border. CompoundBorder( new javax .swing .border .TitledBorder (new javax. swing. border. EmptyBorder
            ( 0, 0, 0, 0) , "JF\u006frm\u0044es\u0069gn\u0065r \u0045va\u006cua\u0074io\u006e", javax. swing. border. TitledBorder. CENTER, javax. swing. border
            . TitledBorder. BOTTOM, new java .awt .Font ("D\u0069al\u006fg" ,java .awt .Font .BOLD ,12 ), java. awt
            . Color. red) ,panel1. getBorder( )) ); panel1. addPropertyChangeListener (new java. beans. PropertyChangeListener( ){ @Override public void
            propertyChange (java .beans .PropertyChangeEvent e) {if ("\u0062or\u0064er" .equals (e .getPropertyName () )) throw new RuntimeException( )
            ; }} );

            GroupLayout panel1Layout = new GroupLayout(panel1);
            panel1.setLayout(panel1Layout);
            panel1Layout.setHorizontalGroup(
                panel1Layout.createParallelGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
            );
            panel1Layout.setVerticalGroup(
                panel1Layout.createParallelGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
            );
        }

        //---- label3 ----
        label3.setText("text");
        label3.setIcon(new ImageIcon(getClass().getResource("/pl/wipb/ztp/ps2/magazyn.jpg")));
        label3.setName("label3");

        //---- zalogujBtn ----
        zalogujBtn.setText("Zaloguj");
        zalogujBtn.setBackground(new Color(153, 153, 255));
        zalogujBtn.setName("zalogujBtn");

        //---- zarejestrujLabel ----
        zarejestrujLabel.setText("Nie masz konta? Zarejestruj si\u0119!");
        zarejestrujLabel.setName("zarejestrujLabel");
        zarejestrujLabel.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                zarejestrujLabelMouseClicked(e);
            }
        });

        GroupLayout contentPaneLayout = new GroupLayout(contentPane);
        contentPane.setLayout(contentPaneLayout);
        contentPaneLayout.setHorizontalGroup(
            contentPaneLayout.createParallelGroup()
                .addGroup(GroupLayout.Alignment.TRAILING, contentPaneLayout.createSequentialGroup()
                    .addComponent(label3, GroupLayout.PREFERRED_SIZE, 384, GroupLayout.PREFERRED_SIZE)
                    .addGap(24, 24, 24)
                    .addGroup(contentPaneLayout.createParallelGroup()
                        .addComponent(label1, GroupLayout.PREFERRED_SIZE, 72, GroupLayout.PREFERRED_SIZE)
                        .addComponent(emailField, GroupLayout.PREFERRED_SIZE, 328, GroupLayout.PREFERRED_SIZE)
                        .addComponent(label2, GroupLayout.PREFERRED_SIZE, 61, GroupLayout.PREFERRED_SIZE)
                        .addComponent(hasloField, GroupLayout.PREFERRED_SIZE, 328, GroupLayout.PREFERRED_SIZE)
                        .addComponent(zalogujBtn)
                        .addComponent(zarejestrujLabel))
                    .addGap(55, 55, 55)
                    .addComponent(panel1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGap(63, 63, 63))
        );
        contentPaneLayout.setVerticalGroup(
            contentPaneLayout.createParallelGroup()
                .addGroup(contentPaneLayout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(panel1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addContainerGap())
                .addGroup(contentPaneLayout.createSequentialGroup()
                    .addGap(72, 72, 72)
                    .addComponent(label1, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                    .addGap(18, 18, 18)
                    .addComponent(emailField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(18, 18, 18)
                    .addComponent(label2, GroupLayout.PREFERRED_SIZE, 31, GroupLayout.PREFERRED_SIZE)
                    .addGap(18, 18, 18)
                    .addComponent(hasloField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(18, 18, 18)
                    .addComponent(zalogujBtn)
                    .addGap(18, 18, 18)
                    .addComponent(zarejestrujLabel)
                    .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGroup(contentPaneLayout.createSequentialGroup()
                    .addComponent(label3, GroupLayout.PREFERRED_SIZE, 408, GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE))
        );
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - unknown
    private JLabel label1;
    private JTextField emailField;
    private JLabel label2;
    private JPasswordField hasloField;
    private JPanel panel1;
    private JLabel label3;
    private JButton zalogujBtn;
    private JLabel zarejestrujLabel;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
    Biblioteka biblioteka = new Biblioteka();

    public LogowanieForm(String title) {
        super(title);

        initComponents();
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);

        zalogujBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String Email, Haslo;
                Email = emailField.getText();
                Haslo = hasloField.getText();

                for(Uzytkownik u : biblioteka.selectUzytkownik()){
                    if(u.email.equals(Email) && u.haslo.equals(Haslo)){
                        if(u.typKonta == 1){
                            JFrame frame = new SklepForm("Sklep");
                            frame.setVisible(true);
                            dispose();
                        }
                        else
                        {
                            JFrame frame = new StartForm("Start");
                            frame.setVisible(true);
                            dispose();
                        }
                    }
                }
            }
        });


    }
}
