package pl.wipb.ztp.ps2;

import java.util.LinkedList;

public class Paragon implements Rachunek {
    public double cenaDoZaplaty;
    public int id_paragonu;
    public Paragon(int id, double cena){
        cenaDoZaplaty = cena;
        id_paragonu = id;
    }
    public Paragon(){}
    @Override
    public void SposobRozliczenia(Zamowienie zamowienie) {
        System.out.println("Paragon:-----");
        System.out.println(zamowienie.WystawRachunek(this));
    }

    @Override
    public double Rozlicz(LinkedList<Produkt> produkty) {
        cenaDoZaplaty = 0;
        for (Produkt p : produkty){
            cenaDoZaplaty += p.getCena();
        }
        return cenaDoZaplaty;
    }
}
